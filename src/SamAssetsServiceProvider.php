<?php

namespace Totem\SamAssets;

use Illuminate\Support\Facades\File;
use Totem\SamCore\App\Traits\TraitServiceProvider;
use Illuminate\Support\ServiceProvider;

class SamAssetsServiceProvider extends ServiceProvider
{
    use TraitServiceProvider;

    public function getNamespace(): string
    {
        return 'sam-assets';
    }

    public function boot(): void
    {
        $this->loadAndPublish(
            __DIR__ . '/resources/lang',
            __DIR__ . '/database/migrations',
            __DIR__ . '/resources/views/'
        );

        $this->registerEvents();

        if (File::isDirectory(public_path('storage/assets')) === false) {
            File::makeDirectory(public_path('storage/assets'), 0777, true, true);
        }
        if (File::isDirectory(public_path('storage/assets/locations')) === false) {
            File::makeDirectory(public_path('storage/assets/locations'), 0777, true, true);
        }

        $this->loadRoutesFrom(__DIR__.'/routes/api.php');
    }

    public function register(): void
    {
        $this->registerEloquentFactoriesFrom(__DIR__. '/database/factories');

        $this->configureBinding([
            \Totem\SamAssets\App\Repositories\Contracts\LogRepositoryInterface::class => \Totem\SamAssets\App\Repositories\LogRepository::class,
            \Totem\SamAssets\App\Repositories\Contracts\AssetsRepositoryInterface::class => \Totem\SamAssets\App\Repositories\AssetsRepository::class,
            \Totem\SamAssets\App\Repositories\Contracts\HardwareRepositoryInterface::class => \Totem\SamAssets\App\Repositories\HardwareRepository::class,
            \Totem\SamAssets\App\Repositories\Contracts\AccessoriesRepositoryInterface::class => \Totem\SamAssets\App\Repositories\AccessoriesRepository::class,
            \Totem\SamAssets\App\Repositories\Contracts\SoftwareRepositoryInterface::class => \Totem\SamAssets\App\Repositories\SoftwareRepository::class,
            \Totem\SamAssets\App\Repositories\Contracts\LocationRepositoryInterface::class => \Totem\SamAssets\App\Repositories\LocationRepository::class,
            \Totem\SamAssets\App\Repositories\Contracts\CardRepositoryInterface::class => \Totem\SamAssets\App\Repositories\CardRepository::class,
            \Totem\SamAssets\App\Repositories\Contracts\MetaRepositoryInterface::class => \Totem\SamAssets\App\Repositories\MetaRepository::class,
        ]);
    }

    public function registerEvents(): void
    {
        \Totem\SamAssets\App\Model\Log::observe( \Totem\SamAssets\App\Observer\LogObserver::class);

        \Illuminate\Support\Facades\Event::listen(
            [
                \Totem\SamAssets\App\Events\LogAssetAssign::class,
                \Totem\SamAssets\App\Events\LogAssetCheckIn::class,
                \Totem\SamAssets\App\Events\LogAssetCheckOut::class,
                \Totem\SamAssets\App\Events\LogAssetRemove::class,
                \Totem\SamAssets\App\Events\LogAssetWithdrawn::class,
                \Totem\SamAssets\App\Events\LogAssetFileUpload::class,
                \Totem\SamAssets\App\Events\LogAssetFileRemove::class,
            ],
            \Totem\SamAssets\App\Listeners\LogAssetListener::class
        );
    }

}
