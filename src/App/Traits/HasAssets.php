<?php

namespace Totem\SamAssets\App\Traits;

use Totem\SamAssets\App\Classes\StorageCollection;
use Totem\SamAssets\App\Enums\AssetCategory;
use Totem\SamAssets\App\Model\Asset;
use Totem\SamAssets\App\Model\Log;

/**
 * @property \Illuminate\Database\Eloquent\Collection|Asset[] assets
 */
trait HasAssets
{

    public function assignable(): \Illuminate\Database\Eloquent\Relations\MorphTo
    {
        return $this->morphTo();
    }

    public function assets(): \Illuminate\Database\Eloquent\Relations\MorphToMany
    {
        return $this->morphToMany(Asset::class, 'assignable', 'asset_assign');
    }

    public function history(): \Illuminate\Database\Eloquent\Relations\MorphMany
    {
        return $this->morphMany(Log::class, 'assignable')->with(['user', 'asset'])->latest();
    }

    public function getHardwareCountAttribute(): int
    {
        return $this->assets->where('category', AssetCategory::Hardware)->count();
    }

    public function getAccessoryCountAttribute(): int
    {
        return $this->assets->where('category', AssetCategory::Accessory)->count();
    }

    public function getSoftwareCountAttribute(): int
    {
        return $this->assets->where('category', AssetCategory::Software)->count();
    }

    public function getAssetsCollectionAttribute(): StorageCollection
    {
        $relation = $this->relationLoaded('assets') ? $this->getRelation('assets') : $this->assets;
        return new StorageCollection($relation->groupBy('category'));
    }
}
