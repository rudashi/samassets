<?php

namespace Totem\SamAssets\App\Traits;

use Totem\SamAssets\App\Model\Meta;

trait HasMeta
{

    public function meta() : \Illuminate\Database\Eloquent\Relations\MorphMany
    {
        return $this->morphMany(Meta::class, 'model');
    }

}