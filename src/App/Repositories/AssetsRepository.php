<?php

namespace Totem\SamAssets\App\Repositories;

use Illuminate\Support\Str;
use Totem\SamAssets\App\Model\Meta;
use Totem\SamAssets\App\Model\Asset;
use Totem\SamAssets\App\Classes\StorageCollection;
use Totem\SamCore\App\Repositories\BaseRepository;
use Totem\SamCore\App\Exceptions\RepositoryException;
use Totem\SamAssets\App\Repositories\Contracts\AssetsRepositoryInterface;

/**
 * @property \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Builder|Asset model
 */
class AssetsRepository extends BaseRepository implements AssetsRepositoryInterface
{

    public function model(): string
    {
        return Asset::class;
    }

    public function getUnused() : StorageCollection
    {
        return new StorageCollection($this->getUnusedNotGrouped()->groupBy('category'));
    }

    public function getUnusedNotGrouped() : \Illuminate\Support\Collection
    {
        return $this->model
            ->doesntHave('locations', 'and', static function(\Illuminate\Database\Eloquent\Builder $query){
                $query->withoutGlobalScope('assignable_type');
            })
            ->where(static function(\Illuminate\Database\Eloquent\Builder $query) {
                $query->where('multi', 1)
                    ->orWhere(static function(\Illuminate\Database\Eloquent\Builder $query) {
                        $query->where('multi', 0)
                            ->doesntHave('users');
                    });
            })
            ->orderBy('name')
            ->get();
    }

    public function findById(int $id = 0, array $columns = ['*']) : Asset
    {
        return $this->findWithRelationsById($id, [], $columns);
    }

    public function findWithRelationsById(int $id = 0, array $relationships = [], array $columns = ['*'])
    {
        if ($id === 0) {
            throw new RepositoryException( __('No asset id have been given.') );
        }

        $data = $this->model->find($id, $columns);

        if ($data === null) {
            throw new RepositoryException( __('Given id :code is invalid or asset not exist.', ['code' => $id]), 404);
        }

        return $data->setParametersIntersect()->loadMissing($relationships);
    }

    public function attach(string $relation, \Illuminate\Database\Eloquent\Model $toModel, $asset) : Asset
    {
        $asset = $asset instanceof Asset ? $asset : $this->findById($asset ?: 0);

        if ($asset->{$relation}->contains($toModel->id)) {
            throw new RepositoryException(__('The asset :name is already assigned.', ['name' => $asset->name]), 422);
        }
        if (!$asset->multi && count($asset->{$relation}) > 0) {
            throw new RepositoryException(__('The asset :name is already assigned to other :relation.', ['name' => $asset->name, 'relation' => $relation]), 422);
        }

        $asset->{$relation}()->save($toModel);
        $asset->load($relation);
        return $asset;
    }

    public function detach(string $relation, \Illuminate\Database\Eloquent\Model $fromModel, $asset) : Asset
    {
        $asset = $asset instanceof Asset ? $asset : $this->findById($asset ?: 0);

        if ($asset->{$relation}->contains($fromModel->id) === false) {
            throw new RepositoryException(__('The asset :name is not assigned.', ['name' => $asset->name]), 422);
        }

        $asset->{$relation}()->detach($fromModel->id);
        $asset->load($relation);
        return $asset;
    }

    public function store(\Illuminate\Http\Request $request, int $id = 0) : Asset
    {
        $asset = ($id === 0) ? $this->model : $this->find($id);

        $asset->fill($request->all());

        if ($id === 0) {
            $asset->image = Str::after($request->input('preview'), 'storage/');
        }
        if ($request->file('image') !== null) {
            $asset->image = $request->file('image')->store('assets', 'public');
        }

        $asset->save();

        return $asset;
    }

    public function getTypesWithParameters(array $types) : array
    {
        foreach ($types as $key => $type) {
            $types[$key] = $type->value::getParameters();
        }
        return $types;
    }

    public function attachMeta(Meta $meta, $asset) : Asset
    {
        $asset = $asset instanceof Asset ? $asset : $this->findById($asset ?: 0);

        $asset->meta()->save($meta);
        $asset->load('meta');

        return $asset;
    }

    public function detachMeta(string $uuid, $asset) : Asset
    {
        $asset = $asset instanceof Asset ? $asset : $this->findById($asset ?: 0);

        if ($asset->meta->contains($uuid) === false) {
            throw new RepositoryException( __('Given id :code is invalid or credentials data not exist.', ['code' => $uuid]), 404);
        }

        $asset->meta()->whereKey($uuid)->delete();
        $asset->load('meta');

        return $asset;
    }

}
