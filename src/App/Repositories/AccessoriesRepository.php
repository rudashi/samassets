<?php

namespace Totem\SamAssets\App\Repositories;

use Totem\SamAssets\App\Model\Accessory;
use Totem\SamAssets\App\Repositories\Contracts\AccessoriesRepositoryInterface;
use Totem\SamCore\App\Exceptions\RepositoryException;

/**
 * @property \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Builder|Accessory model
 */
class AccessoriesRepository extends AssetsRepository implements AccessoriesRepositoryInterface
{

    public function model(): string
    {
        return Accessory::class;
    }

    public function findWithRelationsById(int $id = 0, array $relationships = [], array $columns = ['*']) : Accessory
    {
        if ($id === 0) {
            throw new RepositoryException( __('Accessory ID is not provided.') );
        }

        return parent::findWithRelationsById($id, $relationships, $columns);
    }
}