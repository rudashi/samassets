<?php

namespace Totem\SamAssets\App\Repositories;

use Illuminate\Support\Carbon;
use Totem\SamAssets\App\Model\Log;
use Totem\SamAssets\App\Enums\LogType;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Totem\SamCore\App\Repositories\BaseRepository;
use Totem\SamAssets\App\Repositories\Contracts\LogRepositoryInterface;

/**
 * @property \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Builder|Log model
 */
class LogRepository extends BaseRepository implements LogRepositoryInterface
{

    public function model(): string
    {
        return Log::class;
    }

    public function prepareLatest() : \Illuminate\Database\Eloquent\Builder
    {
        return $this->model->with(['asset', 'user', 'assigned'])->latest();
    }

    public function paginate(int $perPage = 15, array $columns = ['*']): LengthAwarePaginator
    {
        return $this->prepareLatest()->paginate($perPage, $columns);
    }

    public function assetHistory(int $id = 0) : Collection
    {
        return $this->prepareLatest()->where('asset_id', $id)->get();
    }

    public function latest(array $columns = ['*']) : Collection
    {
        return $this->prepareLatest()->get($columns);
    }

    public function grouped(array $columns = ['*']) : \Illuminate\Support\Collection
    {
        return $this->latest()->mapToGroups(static function(Log $item) {
            return [ $item->created_at->format('Y-m-d') => $item ];
        });
    }

    public function protocols() : Collection
    {
        return $this->model->with(['asset', 'assigned'])->whereIn('action', [
            LogType::getKey(LogType::checkIn),
            LogType::getKey(LogType::checkOut)
        ])->latest()->get();
    }

    public function groupProtocols(array $logs): \Illuminate\Support\Collection
    {
        return $this->model->with(['asset', 'assigned'])->findMany($logs)->groupBy(['action', 'assignable_id']);
    }

    public function markAsPrint(array $logs): void
    {
        $now = Carbon::now();
        $this->model->whereIn($this->model->getKeyName(), $logs)->each(static function(Log $model) use ($now) {
            $model->update(['printed_at' => $now]);
        });
    }

}