<?php

namespace Totem\SamAssets\App\Repositories;

use Totem\SamAssets\App\Model\Hardware;
use Totem\SamCore\App\Exceptions\RepositoryException;
use Totem\SamAssets\App\Repositories\Contracts\HardwareRepositoryInterface;

/**
 * @property \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Builder|Hardware model
 */
class HardwareRepository extends AssetsRepository implements HardwareRepositoryInterface
{

    public function model(): string
    {
        return Hardware::class;
    }

    public function findWithRelationsById(int $id = 0, array $relationships = [], array $columns = ['*']) : Hardware
    {
        if ($id === 0) {
            throw new RepositoryException( __('No hardware id have been given.') );
        }

        return parent::findWithRelationsById($id, $relationships, $columns);
    }

}