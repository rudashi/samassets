<?php

namespace Totem\SamAssets\App\Repositories\Contracts;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Totem\SamCore\App\Repositories\Contracts\RepositoryInterface;

interface LogRepositoryInterface extends RepositoryInterface
{

    public function prepareLatest() : \Illuminate\Database\Eloquent\Builder;

    public function paginate(int $perPage = 15, array $columns = ['*']): LengthAwarePaginator;

    public function assetHistory(int $id = 0) : Collection;

    public function latest(array $columns = ['*']) : Collection;

    public function grouped(array $columns = ['*']) : \Illuminate\Support\Collection;

}