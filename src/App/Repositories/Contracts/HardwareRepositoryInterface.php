<?php

namespace Totem\SamAssets\App\Repositories\Contracts;

use Totem\SamAssets\App\Model\Hardware;

interface HardwareRepositoryInterface extends AssetsRepositoryInterface
{

    public function findWithRelationsById(int $id = 0, array $relationships = [], array $columns = ['*']) : Hardware;

}