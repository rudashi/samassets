<?php

namespace Totem\SamAssets\App\Repositories\Contracts;

use Totem\SamAssets\App\Model\Software;

interface SoftwareRepositoryInterface extends AssetsRepositoryInterface
{

    public function findWithRelationsById(int $id = 0, array $relationships = [], array $columns = ['*']) : Software;

}