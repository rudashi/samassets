<?php

namespace Totem\SamAssets\App\Repositories\Contracts;

use Totem\SamAssets\App\Model\Meta;
use Totem\SamCore\App\Repositories\Contracts\RepositoryInterface;

interface MetaRepositoryInterface extends RepositoryInterface
{

    public function make(?\Illuminate\Http\Request $request) : Meta;

    public function storeUUID(\Illuminate\Http\Request $request, string $uuid = null) : Meta;

}