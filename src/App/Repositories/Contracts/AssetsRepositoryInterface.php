<?php

namespace Totem\SamAssets\App\Repositories\Contracts;

use Totem\SamAssets\App\Model\Asset;
use Totem\SamCore\App\Repositories\Contracts\RepositoryInterface;

interface AssetsRepositoryInterface extends RepositoryInterface
{

    public function findById(int $id = 0, array $columns = ['*']): Asset;

    public function getUnused(): \Totem\SamAssets\App\Classes\StorageCollection;

    public function getUnusedNotGrouped(): \Illuminate\Support\Collection;

    public function attach(string $relation, \Illuminate\Database\Eloquent\Model $toModel, $asset): Asset;

    public function detach(string $relation, \Illuminate\Database\Eloquent\Model $fromModel, $asset): Asset;

    public function getTypesWithParameters(array $types): array;

}
