<?php

namespace Totem\SamAssets\App\Repositories\Contracts;

use Totem\SamAssets\App\Model\Location;
use Totem\SamCore\App\Repositories\Contracts\RepositoryInterface;

interface LocationRepositoryInterface extends RepositoryInterface
{

    public function store(\Illuminate\Http\Request $request, int $id = 0) : Location;

    public function findById(int $id = 0, array $columns = ['*']) : Location;

}