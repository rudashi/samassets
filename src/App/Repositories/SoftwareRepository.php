<?php

namespace Totem\SamAssets\App\Repositories;

use Totem\SamAssets\App\Model\Software;
use Totem\SamCore\App\Exceptions\RepositoryException;
use Totem\SamAssets\App\Repositories\Contracts\SoftwareRepositoryInterface;

/**
 * @property \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Builder|Software model
 */
class SoftwareRepository extends AssetsRepository implements SoftwareRepositoryInterface
{

    public function model(): string
    {
        return Software::class;
    }

    public function findWithRelationsById(int $id = 0, array $relationships = [], array $columns = ['*']) : Software
    {
        if ($id === 0) {
            throw new RepositoryException( __('No software id have been given.') );
        }

        $data = $this->model->with($relationships)->find($id, $columns);

        if ($data === null) {
            throw new RepositoryException(  __('Given id :code is invalid or software not exist.', ['code' => $id]), 404);
        }

        return $data;
    }

}