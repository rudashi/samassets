<?php

namespace Totem\SamAssets\App\Repositories;

use Totem\SamAssets\App\Model\Location;
use Totem\SamCore\App\Exceptions\RepositoryException;
use Totem\SamCore\App\Repositories\BaseRepository;
use Totem\SamAssets\App\Repositories\Contracts\LocationRepositoryInterface;

/**
 * @property \Illuminate\Database\Eloquent\Builder|Location model
 */
class LocationRepository extends BaseRepository implements LocationRepositoryInterface
{

    public function model(): string
    {
        return Location::class;
    }

    public function store(\Illuminate\Http\Request $request, int $id = 0) : Location
    {
        $location = ($id === 0) ? $this->model : $this->find($id);

        $location->fill($request->all());

        if ($request->file('image') !== null) {
            $location->image = $request->file('image')->store('assets/locations', 'public');
        }

        $location->save();

        return $location;
    }

    public function findWithRelationsById(int $id = 0, array $relationships = [], array $columns = ['*']) : Location
    {
        if ($id === 0) {
            throw new RepositoryException( __('No asset location id have been given.') );
        }

        $data = $this->model->with($relationships)->find($id, $columns);

        if ($data === null) {
            throw new RepositoryException(  __('Given id :code is invalid or asset location not exist.', ['code' => $id]), 404);
        }

        return $data;
    }

    public function findById(int $id = 0, array $columns = ['*']) : Location
    {
        return $this->findWithRelationsById($id, [], $columns);
    }

}