<?php

namespace Totem\SamAssets\App\Repositories;

use Totem\SamAssets\App\Model\Card;
use Totem\SamCore\App\Exceptions\RepositoryException;
use Totem\SamAssets\App\Repositories\Contracts\CardRepositoryInterface;

/**
 * @property \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Builder|Card model
 */
class CardRepository extends AssetsRepository implements CardRepositoryInterface
{

    public function model(): string
    {
        return Card::class;
    }

    public function findWithRelationsById(int $id = 0, array $relationships = [], array $columns = ['*']) : Card
    {
        if ($id === 0) {
            throw new RepositoryException( __('No card id have been given.') );
        }

        return parent::findWithRelationsById($id, $relationships, $columns);
    }

}
