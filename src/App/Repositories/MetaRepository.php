<?php

namespace Totem\SamAssets\App\Repositories;

use Totem\SamAssets\App\Model\Meta;
use Totem\SamCore\App\Repositories\BaseRepository;
use Totem\SamAssets\App\Repositories\Contracts\MetaRepositoryInterface;

/**
 * @property \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Builder|Meta model
 */
class MetaRepository extends BaseRepository implements MetaRepositoryInterface
{

    public function model(): string
    {
        return Meta::class;
    }

    public function make(?\Illuminate\Http\Request $request) : Meta
    {
        $meta = $this->model;

        if ($request) {
            $meta->fill($request->all());
        }
        return $meta;
    }

    public function storeUUID(\Illuminate\Http\Request $request, string $uuid = null) : Meta
    {
        $meta = ($uuid === null) ? $this->model : $this->findBy('id', $uuid);

        $meta->fill($request->all());
        $meta->save();

        return $meta;
    }

}