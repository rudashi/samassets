<?php

namespace Totem\SamAssets\App\Resources;

use Totem\SamCore\App\Resources\ApiCollection;

class ActivityCollection extends ApiCollection
{

    public $collects = ActivityResource::class;

}
