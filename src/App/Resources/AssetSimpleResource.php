<?php

namespace Totem\SamAssets\App\Resources;

use Totem\SamCore\App\Resources\ApiResource;

class AssetSimpleResource extends ApiResource
{

    public function toArray($request) : array
    {
        return [
            'id'                => $this->id,
            'name'              => $this->name,
            'type'              => $this->category,
            'category'          => $this->category_name,
            'serial_number'     => $this->serial_number,
        ];
    }

}
