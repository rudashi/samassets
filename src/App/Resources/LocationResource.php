<?php

namespace Totem\SamAssets\App\Resources;

use Totem\SamCore\App\Resources\ApiResource;
use Totem\SamUsersBundle\App\Resources\UserResource;

/**
 * @property \Totem\SamAssets\App\Model\Location $resource
 */
class LocationResource extends ApiResource
{

    public function toArray($request) : array
    {
        return [
            'id'            => $this->resource->id,
            'name'          => $this->resource->name,
            'parent_id'     => $this->resource->parent_id,
            'parent'        => self::make($this->whenLoaded('parent')),
            'street'        => $this->resource->street,
            'street_number' => $this->resource->street_number,
            'place_number'  => $this->resource->place_number,
            'post_code'     => $this->resource->post_code,
            'city'          => $this->resource->city,
            'country_code'  => $this->resource->country_code,
            'manager_id'    => $this->resource->manager_id,
            'manager'       => UserResource::make($this->whenLoaded('manager')),
            'image'         => $this->resource->image,
            'assets_count'  => $this->when($this->resource->relationLoaded('assets') || $this->resource->assets_count !== null, function() {
                return $this->resource->assets_count ?? $this->resource->assets->count();
            }),
            'assets'        => $this->when($this->resource->relationLoaded('assets'), AssetCollection::make($this->resource->assets)),
            'history'       => $this->when($this->resource->relationLoaded('history'), ActivityCollection::make($this->resource->history)),
        ];
    }

}
