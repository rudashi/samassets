<?php

namespace Totem\SamAssets\App\Resources;

use Totem\SamCore\App\Resources\ApiCollection;

class LocationCollection extends ApiCollection
{

    public $collects = LocationResource::class;

}
