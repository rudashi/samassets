<?php

namespace Totem\SamAssets\App\Resources;

use Totem\SamCore\App\Resources\ApiCollection;

class ActivityGroupedCollection extends ApiCollection
{

    public $collects = ActivityCollection::class;

}
