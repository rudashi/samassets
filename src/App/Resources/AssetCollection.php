<?php

namespace Totem\SamAssets\App\Resources;

use Totem\SamCore\App\Resources\ApiCollection;

class AssetCollection extends ApiCollection
{

    public $collects = \Totem\SamAssets\App\Resources\AssetResource::class;

}
