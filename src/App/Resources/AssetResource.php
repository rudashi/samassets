<?php

namespace Totem\SamAssets\App\Resources;

use Totem\SamAssets\App\Model\Asset;
use Totem\SamCore\App\Resources\ApiResource;
use Totem\SamCore\App\Resources\FileCollection;
use Totem\SamUsersBundle\App\Resources\UserCollection;

/** @property Asset $resource */
class AssetResource extends ApiResource
{

    public function toArray($request) : array
    {
        return [
            'id'                => $this->resource->id,
            'name'              => $this->resource->name,
            'type'              => $this->resource->category,
            'category'          => $this->resource->category_name,
            'serial_number'     => $this->resource->serial_number,
            'parameters'        => $this->when($this->resource->parameters->isNotEmpty(), $this->resource->parameters, new \stdClass()),
            'meta'              => $this->when($this->resource->relationLoaded('meta'), $this->resource->meta),
            'image'             => $this->resource->image,
            'invoice_number'    => $this->resource->invoice_number,
            'invoice_date'      => $this->resource->invoice_date,
            'description'       => $this->resource->description,
            'multi'             => $this->resource->multi,
            'active'            => $this->resource->active,
            'users'             => UserCollection::make($this->whenLoaded('users')),
            'locations'         => LocationCollection::make($this->whenLoaded('locations')),
            'history'           => ActivityCollection::make($this->whenLoaded('history')),
            'files'             => FileCollection::make($this->whenLoaded('files')),
        ];
    }

}
