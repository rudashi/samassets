<?php

namespace Totem\SamAssets\App\Resources;

use Totem\SamCore\App\Resources\ApiResource;

class ActivityResource extends ApiResource
{

    public function toArray($request) : array
    {
        return [
            'id'            => $this->id,
            'created_at'    => (string) $this->created_at,
            'date'          => $this->created_at->format('Y-m-d'),
            'time'          => $this->created_at->format('H:i:s'),
            'action'        => $this->action,
            'asset'         => $this->when($this->resource->relationLoaded('asset'), AssetSimpleResource::make($this->asset)),
            'user'          => $this->when($this->resource->relationLoaded('user'), $this->user),
            'assigned'      => $this->when($this->resource->relationLoaded('assigned'), $this->assigned),
            'printed_at'    => $this->printed_at,
        ];
    }

}
