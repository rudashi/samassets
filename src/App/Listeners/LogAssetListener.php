<?php

namespace Totem\SamAssets\App\Listeners;

use Totem\SamAssets\App\Model\Log;
use Totem\SamAssets\App\Events\Contracts\LogAssetContract;

class LogAssetListener
{

    public function handle(LogAssetContract $event) : void
    {
        $log = new Log();
        $log->fill([
            'action'            => $event->getAction(),
            'user_id'           => auth()->id(),
            'asset_id'          => $event->getAssetId(),
            'assignable_type'   => $event->getAssignableType(),
            'assignable_id'     => $event->getAssignableId(),
        ]);

        $log->save();
    }

    public function failed(LogAssetContract $event, \Exception $exception) : void
    {
        //
    }

}
