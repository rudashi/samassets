<?php

namespace Totem\SamAssets\App\Events\Contracts;

interface LogAssetContract
{

    public function setAction(string $action) : LogAssetContract;

    public function getAction() : string;

    public function getAssetId() : int;

    public function getAssignableType() : string;

    public function getAssignableId() : int;

}