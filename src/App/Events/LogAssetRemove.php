<?php

namespace Totem\SamAssets\App\Events;

use Totem\SamAssets\App\Enums\LogType;
use Totem\SamAssets\App\Model\Asset;
use Illuminate\Database\Eloquent\Model;

class LogAssetRemove extends LogAsset
{

    public function __construct(Model $assignModel, Asset $asset)
    {
        parent::__construct($assignModel, $asset);

        $this->setAction(LogType::getKey(LogType::remove));
    }

}