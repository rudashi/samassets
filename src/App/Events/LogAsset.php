<?php

namespace Totem\SamAssets\App\Events;

use Totem\SamAssets\App\Model\Asset;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Queue\SerializesModels;
use Totem\SamAssets\App\Events\Contracts\LogAssetContract;

abstract class LogAsset implements LogAssetContract
{

    use SerializesModels;

    public string $action;
    private int $asset_id;
    public string $assignable_type;
    private int $assignable_id;

    public function __construct(Model $assignModel, Asset $asset)
    {
        $this->asset_id         = $asset->id;
        $this->assignable_type  = get_class($assignModel);
        $this->assignable_id    = $assignModel->id;
    }

    public function setAction(string $action): LogAssetContract
    {
        $this->action = $action;

        return $this;
    }

    public function getAction(): string
    {
        return $this->action;
    }

    public function getAssetId(): int
    {
        return $this->asset_id;
    }

    public function getAssignableType(): string
    {
        return $this->assignable_type;
    }

    public function getAssignableId(): int
    {
        return $this->assignable_id;
    }
}
