<?php

namespace Totem\SamAssets\App\Requests;

use Totem\SamAssets\App\Enums\MetaType;
use Totem\SamCore\App\Requests\BaseRequest;

class MetaRequest extends BaseRequest
{
    public function rules() : array
    {
        return [
            'key'           => 'required|enum_value:'.MetaType::class,
            'login'         => 'nullable',
            'password'      => 'nullable',
            'description'   => 'nullable',
        ];
    }

    public function messages() : array
    {
        return [
            'enum_value' => __('The :attribute field value you have entered is invalid.'),
        ];
    }
}