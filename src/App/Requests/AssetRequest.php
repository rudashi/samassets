<?php

namespace Totem\SamAssets\App\Requests;

use Totem\SamAssets\App\Enums\AssetCategory;
use Totem\SamCore\App\Requests\BaseRequest;

class AssetRequest extends BaseRequest
{
    public function rules() : array
    {
        return [
            'name'                  => 'required',
            'category'              => 'required|enum_key:'.AssetCategory::class,
            'type'                  => 'required',
            'serial_number'         => 'nullable',
            'parameters'            => 'present|array',
            'meta'                  => 'nullable',
            'meta.*.key'            => 'nullable|required_with:meta.*.login,meta.*.pass,meta.*.description',
            'meta.*.login'          => 'nullable|required_with:meta.*.key,meta.*.pass,meta.*.description',
            'meta.*.pass'           => 'nullable|required_with:meta.*.key,meta.*.login,meta.*.description',
            'meta.*.description'    => 'nullable|required_with:meta.*.key,meta.*.login,meta.*.pass',
            'image'                 => 'file|nullable',
            'invoice_number'        => 'nullable',
            'invoice_date'          => 'nullable',
            'description'           => 'nullable',
            'multi'                 => 'boolean|nullable',
            'active'                => 'boolean|nullable',
        ];
    }

    protected function prepareForValidation() : void
    {
        $this->merge([
            'multi' => filter_var($this->input('multi'), FILTER_VALIDATE_BOOLEAN),
            'active' => filter_var($this->input('active'), FILTER_VALIDATE_BOOLEAN),
        ]);
        if ($this->has('parameters')) {
            $this->merge([
                'parameters' => is_array($this->input('parameters'))
                    ? $this->input('parameters')
                    : json_decode($this->input('parameters'), true),
            ]);
        }
        foreach ($this->all() as $key => $value) {
            if (is_array($value) === false && strtolower(trim($value)) === 'null') {
                $this->merge([$key => null]);
            }
        }
    }

    public function messages() : array
    {
        return [
            'enum_key' => __('The :attribute field value you have entered is invalid.'),
            'meta.*.required_with' => __('The :attribute field is required when meta parameters are provided.'),
        ];
    }
}

