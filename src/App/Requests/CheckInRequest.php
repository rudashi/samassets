<?php

namespace Totem\SamAssets\App\Requests;

use Totem\SamAssets\App\Enums\AssetConditionType;
use Totem\SamCore\App\Requests\BaseRequest;

class CheckInRequest extends BaseRequest
{
    public function rules() : array
    {
        return [
            'user_id' => 'required',
            'assets' => 'required|array',
            'condition' => 'required|enum_value:'. AssetConditionType::class .', false',
        ];
    }

}