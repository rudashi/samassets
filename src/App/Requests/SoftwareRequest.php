<?php

namespace Totem\SamAssets\App\Requests;

use BenSampo\Enum\Rules\EnumKey;
use Totem\SamAssets\App\Enums\AssetCategory;
use Totem\SamAssets\App\Enums\SoftwareType;

/**
 * @todo https://github.com/BenSampo/laravel-enum/issues/104
 */
class SoftwareRequest extends AssetRequest
{

    public function rules(): array
    {
        return array_replace(
            parent::rules(),
            [
//                'type' => [ 'required', new EnumKey(HardwareType::class) ],
                'type' => 'required|enum_key:'.SoftwareType::class,
            ]
        );
    }

    protected function prepareForValidation() : void
   {
       $this->merge([
           'category' => AssetCategory::getKey(AssetCategory::Software),
       ]);

       parent::prepareForValidation();
   }

}