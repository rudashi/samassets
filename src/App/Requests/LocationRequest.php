<?php

namespace Totem\SamAssets\App\Requests;

use Totem\SamCore\App\Requests\BaseRequest;

class LocationRequest extends BaseRequest
{
    public function rules() : array
    {
        return [
            'name'          => 'required',
            'parent_id'     => 'integer|nullable',
            'manager_id'    => 'integer|nullable',
            'street'        => 'nullable',
            'street_number' => 'nullable',
            'place_number'  => 'nullable',
            'post_code'     => 'nullable',
            'city'          => 'nullable',
            'country_code'  => 'nullable',
            'image'         => 'file|nullable'
        ];
    }

    protected function prepareForValidation(): void
    {
        if ($this->has('parent_id') === false) {
            $this->merge(['parent_id' => null]);
        }
        if ($this->has('manager_id') === false) {
            $this->merge(['manager_id' => null]);
        }

        parent::prepareForValidation();
    }
}