<?php

namespace Totem\SamAssets\App\Requests;

use Totem\SamAssets\App\Enums\AssetCategory;
use Totem\SamAssets\App\Enums\CardType;

class CardRequest extends AssetRequest
{

    public function rules(): array
    {
        return array_replace(
            parent::rules(),
            [
                'type' => 'required|enum_key:' . CardType::class,
            ]
        );
    }

    protected function prepareForValidation() : void
   {
       $this->merge([
           'category' => AssetCategory::getKey(AssetCategory::Card),
       ]);

       parent::prepareForValidation();
   }

}