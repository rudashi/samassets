<?php

namespace Totem\SamAssets\App\Requests;

use BenSampo\Enum\Rules\EnumKey;
use Totem\SamAssets\App\Enums\AssetCategory;
use Totem\SamAssets\App\Enums\HardwareType;

/**
 * @todo https://github.com/BenSampo/laravel-enum/issues/104
 */
class HardwareRequest extends AssetRequest
{

    public function rules(): array
    {
        return array_replace(
            parent::rules(),
            [
//                'type' => [ 'required', new EnumKey(HardwareType::class) ],
                'type' => 'required|enum_key:'.HardwareType::class,
            ]
        );
    }

    protected function prepareForValidation() : void
   {
       $this->merge([
           'category' => AssetCategory::getKey(AssetCategory::Hardware),
       ]);

       parent::prepareForValidation();
   }

}