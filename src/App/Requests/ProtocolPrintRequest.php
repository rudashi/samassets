<?php

namespace Totem\SamAssets\App\Requests;

use Totem\SamCore\App\Requests\BaseRequest;

class ProtocolPrintRequest extends BaseRequest
{
    public function rules() : array
    {
        return [
            'logs' => 'required|array',
        ];
    }

}