<?php

namespace Totem\SamAssets\App\Requests;

use BenSampo\Enum\Rules\EnumKey;
use Totem\SamAssets\App\Enums\AssetCategory;
use Totem\SamAssets\App\Enums\AccessoryType;

/**
 * @todo https://github.com/BenSampo/laravel-enum/issues/104
 */
class AccessoryRequest extends AssetRequest
{

    public function rules(): array
    {
        return array_replace(
            parent::rules(),
            [
//                'type' => [ 'required', new EnumKey(HardwareType::class) ],
                'type' => 'required|enum_key:'.AccessoryType::class,
            ]
        );
    }

    protected function prepareForValidation() : void
   {
       $this->merge([
           'category' => AssetCategory::getKey(AssetCategory::Accessory),
       ]);

       parent::prepareForValidation();
   }

}