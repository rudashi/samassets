<?php

namespace Totem\SamAssets\App\Enums;

use BenSampo\Enum\Enum;

class LogType extends Enum
{

    public const checkIn     = 'checked in';
    public const checkOut    = 'checked out';
    public const assign      = 'assigned';
    public const withdrawn   = 'withdrawn';
    public const remove      = 'removed';
    public const fileUpload  = 'file uploaded';
    public const fileRemove  = 'file removed';

}

