<?php

namespace Totem\SamAssets\App\Enums;

use BenSampo\Enum\Enum;

class HardwareType extends Enum
{
    public const Aio        = \Totem\SamAssets\App\Model\Types\Aio::class;
    public const Desktop    = \Totem\SamAssets\App\Model\Types\Desktop::class;
    public const MacBook    = \Totem\SamAssets\App\Model\Types\MacBook::class;
    public const Notebook   = \Totem\SamAssets\App\Model\Types\Notebook::class;
    public const Phone      = \Totem\SamAssets\App\Model\Types\Phone::class;
    public const Tablet     = \Totem\SamAssets\App\Model\Types\Tablet::class;

}
