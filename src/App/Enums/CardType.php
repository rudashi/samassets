<?php

namespace Totem\SamAssets\App\Enums;

use BenSampo\Enum\Enum;

final class CardType extends Enum
{

    public const Keychain   = \Totem\SamAssets\App\Model\Types\Keychain::class;
    public const KeyCard    = \Totem\SamAssets\App\Model\Types\KeyCard::class;
    public const Key        = \Totem\SamAssets\App\Model\Types\Key::class;

}
