<?php

namespace Totem\SamAssets\App\Enums;

use BenSampo\Enum\Enum;

class AssetConditionType extends Enum
{

    public const Used    = 0;
    public const New     = 1;
    public const Broken  = 2;

}

