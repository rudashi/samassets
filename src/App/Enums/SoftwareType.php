<?php

namespace Totem\SamAssets\App\Enums;

use BenSampo\Enum\Enum;

class SoftwareType extends Enum
{

    public const subscription   = \Totem\SamAssets\App\Model\Types\SubscriptionLicense::class;
    public const license        = \Totem\SamAssets\App\Model\Types\License::class;

}
