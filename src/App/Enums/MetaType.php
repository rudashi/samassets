<?php

namespace Totem\SamAssets\App\Enums;

use BenSampo\Enum\Enum;

final class MetaType extends Enum
{

    public const Account  = 'account';
    public const Mail  = 'mail';
    public const ActiveDirectory  = 'ad';

}
