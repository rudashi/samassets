<?php

namespace Totem\SamAssets\App\Enums;

use BenSampo\Enum\Enum;

final class AssetCategory extends Enum
{

    public const Accessory  = 'Accessories';
    public const Hardware   = 'Hardware';
    public const Software   = 'Software';
    public const Card       = 'Card';

}
