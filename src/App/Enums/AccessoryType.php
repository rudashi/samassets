<?php

namespace Totem\SamAssets\App\Enums;

use BenSampo\Enum\Enum;

class AccessoryType extends Enum
{

    public const Adapter        = \Totem\SamAssets\App\Model\Types\Adapter::class;
    public const Cable          = \Totem\SamAssets\App\Model\Types\Cable::class;
    public const Keyboard       = \Totem\SamAssets\App\Model\Types\Keyboard::class;
    public const Monitor        = \Totem\SamAssets\App\Model\Types\Monitor::class;
    public const SmartTv        = \Totem\SamAssets\App\Model\Types\SmartTv::class;
    public const Mouse          = \Totem\SamAssets\App\Model\Types\Mouse::class;
    public const SimCard        = \Totem\SamAssets\App\Model\Types\SimCard::class;
    public const FuelCard       = \Totem\SamAssets\App\Model\Types\FuelCard::class;
    public const GateRemote     = \Totem\SamAssets\App\Model\Types\GateRemote::class;
    public const PrinterMachine = \Totem\SamAssets\App\Model\Types\PrinterMachine::class;
    public const Others         = \Totem\SamAssets\App\Model\Types\Others::class;

}
