<?php

namespace Totem\SamAssets\App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\HtmlString;
use Totem\SamAssets\App\Model\Log;

class AssetLogCreated extends Notification implements ShouldQueue
{
    use Queueable;

    public Log $log;

    public function __construct(Log $log)
    {
        $this->log = $log->load('asset', 'assigned');
    }

    public function via(): array
    {
        return [
            'mail',
//            'database'
        ];
    }

    public function toMail(): MailMessage
    {
        $from = $this->getAssignedName();
        $asset = __($this->log->asset->type).': '.$this->log->asset->name;

        return (new MailMessage)
            ->subject('[SAM] Zasób '.$this->log->asset->category_name.' - '.$this->log->asset->name)
            ->greeting('Witaj')
            ->line(new HtmlString('<br>'))
            ->line(new HtmlString('<strong>'.__($this->log->action).'</strong> zasób <strong>'.$asset.'</strong> z '.$from) )
            ->line('Aktualny stan zasobu można sprawdzić na profilu zasobu')
            ->line('Prosimy nie odpowiadać na tę wiadomość, ponieważ została wygenerowana automatycznie.')
        ;
    }

    public function toDatabase(): array
    {
        return [
            'id'            => $this->log->id,
            'action'        => $this->log->action,
            'created_at'    => $this->log->created_at,
            'name'          => $this->log->asset->name,
            'category'      => $this->log->asset->category,
            'category_name' => $this->log->asset->category_name,
            'serial_number'  => $this->log->asset->serial_number,
        ];
    }

    private function getAssignedName(): string
    {
        if ($this->log->assigned->firstname) {
            return $this->log->assigned->fullname;
        }
        return $this->log->assigned->name;
    }

}
