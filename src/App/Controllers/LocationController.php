<?php

namespace Totem\SamAssets\App\Controllers;

use Illuminate\Http\Request;
use Totem\SamAssets\App\Enums\LogType;
use Totem\SamCore\App\Resources\ApiResource;
use Totem\SamAssets\App\Resources\AssetResource;
use Totem\SamAssets\App\Requests\LocationRequest;
use Totem\SamAssets\App\Resources\LocationResource;
use Totem\SamAssets\App\Resources\LocationCollection;
use Totem\SamAssets\App\Repositories\Contracts\AssetsRepositoryInterface;
use Totem\SamAssets\App\Repositories\Contracts\LocationRepositoryInterface;
use Totem\SamCore\App\Controllers\ApiController;

class LocationController extends ApiController
{

    public function __construct(LocationRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function index(): LocationCollection
    {
        return new LocationCollection(
            $this->getFromRequestQuery($this->repository->all())
        );
    }

    public function show(int $id): LocationResource
    {
        return new LocationResource(
            $this->getFromRequestQuery($this->repository->findById($id))
        );
    }

    public function create(LocationRequest $request): LocationResource
    {
        return new LocationResource($this->repository->store($request));
    }

    public function replace(int $id, LocationRequest $request): LocationResource
    {
        return new LocationResource($this->repository->store($request, $id));
    }

    public function destroy(int $id): LocationResource
    {
        return new LocationResource($this->repository->delete($id));
    }

    public function update(int $id, Request $request, AssetsRepositoryInterface $assets): ApiResource
    {
        $data = $this->repository->findById($id);

        switch ($request->get('action')) {
            case LogType::getKey(LogType::assign):
                return new AssetResource($assets->attach('locations', $data, $request->input('asset_id')));
            case LogType::getKey(LogType::withdrawn):
                return new AssetResource($assets->detach('locations', $data, $request->input('asset_id')));
            default :
                return $this->response($this->error(422,'Missing Parameter.'));
        }
    }

}
