<?php

namespace Totem\SamAssets\App\Controllers;

use Totem\SamAssets\App\Repositories\Contracts\LogRepositoryInterface;
use Totem\SamAssets\App\Resources\ActivityCollection;
use Totem\SamAssets\App\Resources\ActivityGroupedCollection;
use Totem\SamCore\App\Controllers\ApiController;

class ActivityController extends ApiController
{

    public function __construct(LogRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function index() : ActivityCollection
    {
        return new ActivityCollection($this->getFromRequestQuery($this->repository->prepareLatest()));
    }

    public function grouped() : ActivityGroupedCollection
    {
        return new ActivityGroupedCollection($this->getFromRequestQuery($this->repository->grouped(), [ 'sort' => ['id' => 'desc'] ]));
    }

}
