<?php

namespace Totem\SamAssets\App\Controllers;

use Illuminate\Support\Str;
use Illuminate\Support\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Foundation\Application;
use Totem\SamAssets\App\Requests\ProtocolPrintRequest;
use Totem\SamAssets\App\Resources\ActivityCollection;
use Totem\SamAssets\App\Repositories\Contracts\LogRepositoryInterface;
use Totem\SamCore\App\Controllers\ApiController;

class ProtocolController extends ApiController
{

    public function __construct(LogRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function index() : ActivityCollection
    {
        return new ActivityCollection($this->getFromRequestQuery($this->repository->protocols()));
    }

    public function printing(ProtocolPrintRequest $request): JsonResponse
    {
        $this->repository->markAsPrint($request->input('logs'));

        return new JsonResponse(null, 200);
    }

    public function streamPDF(ProtocolPrintRequest $request, Application $application): \Illuminate\Http\Response
    {
        $activities = $this->repository->groupProtocols($request->input('logs'));

        /** @var $pdf \Barryvdh\Snappy\PdfWrapper */
        $pdf = $application->make(\Barryvdh\Snappy\PdfWrapper::class);
        $pdf->loadView('sam-assets::pdf.protocol', [
                'date' => Carbon::now(),
                'activities' => $activities,
            ])
            ->setOptions([
                'page-size' => 'A4',
                'header-html' => view('sam-assets::pdf.header'),
                'footer-html' => view('sam-assets::pdf.footer')
            ])
        ;
        return $pdf->inline(Str::kebab('protocol-'.Carbon::now()).'.pdf');
    }

}
