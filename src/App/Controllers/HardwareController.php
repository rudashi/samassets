<?php

namespace Totem\SamAssets\App\Controllers;

use Totem\SamAssets\App\Enums\HardwareType;
use Totem\SamAssets\App\Resources\AssetResource;
use Totem\SamAssets\App\Requests\HardwareRequest;
use Totem\SamCore\App\Resources\ApiResource;
use Totem\SamUsers\App\Repositories\Contracts\UserRepositoryInterface;
use Totem\SamAssets\App\Repositories\Contracts\MetaRepositoryInterface;
use Totem\SamAssets\App\Repositories\Contracts\HardwareRepositoryInterface;

/**@property HardwareRepositoryInterface $repository */
class HardwareController extends AssetsController
{

    public function __construct(HardwareRepositoryInterface $repository, UserRepositoryInterface $userRepository, MetaRepositoryInterface $metaRepository)
    {
        parent::__construct($repository, $userRepository, $metaRepository);
    }

    public function create(HardwareRequest $request) : AssetResource
    {
        return new AssetResource($this->repository->store($request));
    }

    public function replace(int $id, HardwareRequest $request) : AssetResource
    {
        return new AssetResource($this->repository->store($request, $id));
    }

    public function getTypes() : ApiResource
    {
        return new ApiResource($this->repository->getTypesWithParameters(HardwareType::getInstances()));
    }

}