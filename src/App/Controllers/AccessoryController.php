<?php

namespace Totem\SamAssets\App\Controllers;

use Totem\SamAssets\App\Enums\AccessoryType;
use Totem\SamAssets\App\Resources\AssetResource;
use Totem\SamAssets\App\Requests\AccessoryRequest;
use Totem\SamCore\App\Resources\ApiResource;
use Totem\SamUsers\App\Repositories\Contracts\UserRepositoryInterface;
use Totem\SamAssets\App\Repositories\Contracts\MetaRepositoryInterface;
use Totem\SamAssets\App\Repositories\Contracts\AccessoriesRepositoryInterface;

/**@property AccessoriesRepositoryInterface $repository */
class AccessoryController extends AssetsController
{

    public function __construct(AccessoriesRepositoryInterface $repository, UserRepositoryInterface $userRepository, MetaRepositoryInterface $metaRepository)
    {
        parent::__construct($repository, $userRepository, $metaRepository);
    }

    public function create(AccessoryRequest $request) : AssetResource
    {
        return new AssetResource($this->repository->store($request));
    }

    public function replace(int $id, AccessoryRequest $request) : AssetResource
    {
        return new AssetResource($this->repository->store($request, $id));
    }

    public function getTypes() : ApiResource
    {
        return new ApiResource($this->repository->getTypesWithParameters(AccessoryType::getInstances()));
    }

}