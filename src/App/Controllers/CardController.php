<?php

namespace Totem\SamAssets\App\Controllers;

use Totem\SamAssets\App\Enums\CardType;
use Totem\SamAssets\App\Requests\CardRequest;
use Totem\SamAssets\App\Resources\AssetResource;
use Totem\SamCore\App\Resources\ApiResource;
use Totem\SamUsers\App\Repositories\Contracts\UserRepositoryInterface;
use Totem\SamAssets\App\Repositories\Contracts\MetaRepositoryInterface;
use Totem\SamAssets\App\Repositories\Contracts\CardRepositoryInterface;

/**@property CardRepositoryInterface $repository */
class CardController extends AssetsController
{

    public function __construct(CardRepositoryInterface $repository, UserRepositoryInterface $userRepository, MetaRepositoryInterface $metaRepository)
    {
        parent::__construct($repository, $userRepository, $metaRepository);
    }

    public function create(CardRequest $request) : AssetResource
    {
        return new AssetResource($this->repository->store($request));
    }

    public function replace(int $id, CardRequest $request) : AssetResource
    {
        return new AssetResource($this->repository->store($request, $id));
    }

    public function getTypes() : ApiResource
    {
        return new ApiResource($this->repository->getTypesWithParameters(CardType::getInstances()));
    }

}