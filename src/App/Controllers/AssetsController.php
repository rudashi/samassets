<?php

namespace Totem\SamAssets\App\Controllers;

use Illuminate\Http\Request;
use Totem\SamAssets\App\Enums\LogType;
use Totem\SamAssets\App\Enums\MetaType;
use Totem\SamAssets\App\Requests\MetaRequest;
use Totem\SamCore\App\Resources\ApiResource;
use Totem\SamAssets\App\Resources\AssetResource;
use Totem\SamCore\App\Controllers\ApiController;
use Totem\SamAssets\App\Resources\AssetCollection;
use Totem\SamUsers\App\Repositories\Contracts\UserRepositoryInterface;
use Totem\SamAssets\App\Repositories\Contracts\MetaRepositoryInterface;
use Totem\SamAssets\App\Repositories\Contracts\AssetsRepositoryInterface;

class AssetsController extends ApiController
{
    private UserRepositoryInterface $userRepository;
    private MetaRepositoryInterface $metaRepository;

    public function __construct(AssetsRepositoryInterface $repository, UserRepositoryInterface $userRepository, MetaRepositoryInterface $metaRepository)
    {
        $this->repository = $repository;
        $this->userRepository = $userRepository;
        $this->metaRepository = $metaRepository;
    }

    public function index(): AssetCollection
    {
        return new AssetCollection(
            $this->getFromRequestQuery($this->repository->all())
        );
    }

    public function show(int $id): AssetResource
    {
        return new AssetResource(
            $this->getFromRequestQuery($this->repository->findById($id))
        );
    }

    public function destroy(int $id): AssetResource
    {
        return new AssetResource($this->repository->delete($id));
    }

    public function update(int $id, Request $request): ApiResource
    {
        switch ($request->get('action')) {
            case LogType::getKey(LogType::checkIn) :
                return new AssetResource($this->repository->attach('users', $this->userRepository->findById($request->input('user_id', 0)), $id));
            case LogType::getKey(LogType::checkOut) :
                return new AssetResource($this->repository->detach('users', $this->userRepository->findById($request->input('user_id', 0)), $id));
            default :
                return $this->response($this->error(422, __('Missing Parameter.')));
        }
    }

    public function unused(): AssetCollection
    {
        return new AssetCollection($this->getFromRequestQuery($this->repository->getUnusedNotGrouped()));
    }

    public function createMeta(string $type, int $id, MetaRequest $request): ApiResource
    {
        return new AssetResource($this->repository->attachMeta($this->metaRepository->make($request), $id));
    }

    public function replaceMeta(string $type, int $id, string $uuid, MetaRequest $request): ApiResource
    {
        return new ApiResource($this->metaRepository->storeUUID($request, $uuid));
    }

    public function destroyMeta(string $type, int $id, string $uuid): ApiResource
    {
        return new AssetResource($this->repository->detachMeta($uuid, $id));
    }

    public function getMetaTypes(): ApiResource
    {
        return new ApiResource(MetaType::getValues());
    }
}
