<?php

namespace Totem\SamAssets\App\Controllers;

use Totem\SamAssets\App\Enums\SoftwareType;
use Totem\SamAssets\App\Resources\AssetResource;
use Totem\SamAssets\App\Requests\SoftwareRequest;
use Totem\SamCore\App\Resources\ApiResource;
use Totem\SamUsers\App\Repositories\Contracts\UserRepositoryInterface;
use Totem\SamAssets\App\Repositories\Contracts\MetaRepositoryInterface;
use Totem\SamAssets\App\Repositories\Contracts\SoftwareRepositoryInterface;

/**@property SoftwareRepositoryInterface $repository */
class SoftwareController extends AssetsController
{

    public function __construct(SoftwareRepositoryInterface $repository, UserRepositoryInterface $userRepository, MetaRepositoryInterface $metaRepository)
    {
        parent::__construct($repository, $userRepository, $metaRepository);
    }

    public function create(SoftwareRequest $request) : AssetResource
    {
        return new AssetResource($this->repository->store($request));
    }

    public function replace(int $id, SoftwareRequest $request) : AssetResource
    {
        return new AssetResource($this->repository->store($request, $id));
    }

    public function getTypes() : ApiResource
    {
        return new ApiResource($this->repository->getTypesWithParameters(SoftwareType::getInstances()));
    }
}