<?php

namespace Totem\SamAssets\App\Controllers;

use Totem\SamCore\App\Resources\FileResource;
use Totem\SamAssets\App\Events\LogAssetFileRemove;
use Totem\SamAssets\App\Events\LogAssetFileUpload;
use Totem\SamCore\App\Requests\FileUploadRequest;
use Totem\SamCore\App\Controllers\ApiController;
use Totem\SamCore\App\Repositories\Contracts\FileInterfaceRepository;
use Totem\SamAssets\App\Repositories\Contracts\AssetsRepositoryInterface;

class AssetUploadController extends ApiController
{
    private FileInterfaceRepository $file;

    public function __construct(AssetsRepositoryInterface $repository, FileInterfaceRepository $file)
    {
        $this->repository = $repository;
        $this->file = $file;
    }

    public function upload(int $id, FileUploadRequest $request)
    {
        try {
            $asset = $this->repository->findById($id);
            $file = $this->file->saveFile($request, $asset);

            event(new LogAssetFileUpload($file, $asset));

            return new FileResource($file);

        } catch (\Exception $exception) {
            return $this->response($this->error(422, $exception->getMessage()));
        }
    }

    public function remove(int $id, int $file_id) : FileResource
    {
        $asset = $this->repository->findById($id);
        $file = $this->file->fileRemove($file_id, $asset);

        event(new LogAssetFileRemove($file, $asset));

        return new FileResource($file);
    }

}
