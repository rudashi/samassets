<?php

namespace Totem\SamAssets\App\Classes;

class LinkElements extends Parameters
{

    private const link_class = 'link-log';

    public function __construct(array $properties = [])
    {
        parent::__construct($this->setProperties($properties));
    }

    private function setProperties(array $properties = [])
    {
        $properties = array_merge(['url' => null, 'name' => null, 'icon' => null, 'class' => null], $properties);
        $properties['class'] .= ($properties['class'] === null ? '' : ' ').self::link_class;

        return $properties;
    }
}