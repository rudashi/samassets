<?php

namespace Totem\SamAssets\App\Classes;

use Illuminate\Support\Collection;

class StorageCollection
{

    public $hardware;
    public $accessories;
    public $software;

    public function __construct(Collection $collection)
    {
        $this->hardware     = $collection->get('Hardware', new Collection());
        $this->accessories  = $collection->get('Accessory', new Collection());
        $this->software     = $collection->get('Software', new Collection());
    }

}