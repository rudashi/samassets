<?php

namespace Totem\SamAssets\App\Observer;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Notification;
use Totem\SamAcl\App\Repositories\Contracts\PermissionRepositoryInterface;
use Totem\SamAssets\App\Enums\AssetCategory;
use Totem\SamAssets\App\Model\Log;
use Totem\SamAssets\App\Notifications\AssetLogCreated;

class LogObserver
{

    private PermissionRepositoryInterface $permission;

    public function __construct(PermissionRepositoryInterface $permission)
    {
        $this->permission = $permission;
    }

    public function created(Log $log): void
    {
        if ($log->asset->category === AssetCategory::getKey(AssetCategory::Card)) {
            Notification::send($this->getUsers('assets.log.notify.card'), new AssetLogCreated($log));
        }
    }

    private function getUsers(string $permission): Collection
    {
        $query = $this->permission->findBySlug($permission, ['users', 'roles.users'], ['id']);
        return $query->users->merge($query->roles->pluck('users')->collapse());
    }

}
