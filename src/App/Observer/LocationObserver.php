<?php

namespace Totem\SamAssets\App\Observer;

use Totem\SamAssets\App\Model\Asset;
use Totem\SamAssets\App\Model\Location;
use Totem\SamAssets\App\Events\LogAssetWithdrawn;
use Totem\SamAssets\App\Repositories\Contracts\AssetsRepositoryInterface;

class LocationObserver
{

    private AssetsRepositoryInterface $asset;

    public function __construct(AssetsRepositoryInterface $asset)
    {
        $this->asset = $asset;
    }

    public function deleting(Location $location) : void
    {
        $location->assets()->detach();
    }

    public function belongsToManyDetached(string $relation, Location $location, array $ids) : void
    {
        $this->asset->findByIds($ids)->map(static function(Asset $asset) use ($location) {
            event(new LogAssetWithdrawn($location, $asset));
        });
    }
}
