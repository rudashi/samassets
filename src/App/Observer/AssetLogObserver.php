<?php

namespace Totem\SamAssets\App\Observer;

use Totem\SamAssets\App\Model\Asset;
use Totem\SamAssets\App\Events\LogAssetAssign;
use Totem\SamAssets\App\Events\LogAssetCheckIn;
use Totem\SamAssets\App\Events\LogAssetCheckOut;
use Totem\SamAssets\App\Events\LogAssetWithdrawn;
use Totem\SamUsers\App\Repositories\Contracts\UserRepositoryInterface;
use Totem\SamAssets\App\Repositories\Contracts\LocationRepositoryInterface;

class AssetLogObserver
{

    private UserRepositoryInterface $user;
    private LocationRepositoryInterface $location;

    public function __construct(UserRepositoryInterface $user, LocationRepositoryInterface $location)
    {
        $this->user = $user;
        $this->location = $location;
    }

    public function morphedByManyAttached(string $relation, Asset $asset, array $ids): void
    {
        switch($relation) {
            case 'users' :
                $this->user->findByIds($ids)->map(static function($user) use ($asset) {
                    event(new LogAssetCheckIn($user, $asset));
                });
                break;
            case 'locations' :
                $this->location->findByIds($ids)->map(static function($location) use ($asset) {
                    event(new LogAssetAssign($location, $asset));
                });
                break;
            default:
                return;
        }
    }

    public function morphedByManyDetached(string $relation, Asset $asset, array $ids): void
    {
        switch($relation) {
            case 'users' :
                $this->user->findByIds($ids)->map(static function($user) use ($asset) {
                    event(new LogAssetCheckOut($user, $asset));
                });
                break;
            case 'locations' :
                $this->location->findByIds($ids)->map(static function($location) use ($asset) {
                    event(new LogAssetWithdrawn($location, $asset));
                });
                break;
            default:
                return;
        }
    }

    public function deleting(Asset $asset): void
    {
        $asset->users()->detach();
        $asset->locations()->detach();
    }

}
