<?php

namespace Totem\SamAssets\App\Model\Contracts;

interface ParameterizableInterface
{

    /**
     * return EnumType::class;
     * @return string|\BenSampo\Enum\Enum
     */
    public function enumType() : string;

}