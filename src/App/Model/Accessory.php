<?php

namespace Totem\SamAssets\App\Model;

use Totem\SamAssets\App\Enums\AccessoryType;
use Chelout\RelationshipEvents\Concerns\HasMorphedByManyEvents;
use Totem\SamAssets\App\Model\Contracts\ParameterizableInterface;

class Accessory extends Asset implements ParameterizableInterface
{

    use HasMorphedByManyEvents;

    public function enumType() : string
    {
        return AccessoryType::class;
    }

}