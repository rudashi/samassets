<?php

namespace Totem\SamAssets\App\Model;

use Totem\SamAssets\App\Enums\SoftwareType;
use Chelout\RelationshipEvents\Concerns\HasMorphedByManyEvents;
use Totem\SamAssets\App\Model\Contracts\ParameterizableInterface;

class Software extends Asset implements ParameterizableInterface
{
    use HasMorphedByManyEvents;

    public function enumType() : string
    {
        return SoftwareType::class;
    }
}