<?php

namespace Totem\SamAssets\App\Model;

use Totem\SamAssets\App\Enums\CardType;
use Chelout\RelationshipEvents\Concerns\HasMorphedByManyEvents;
use Totem\SamAssets\App\Model\Contracts\ParameterizableInterface;

class Card extends Asset implements ParameterizableInterface
{

    use HasMorphedByManyEvents;

    public function enumType() : string
    {
        return CardType::class;
    }

}