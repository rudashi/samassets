<?php

namespace Totem\SamAssets\App\Model;

use Totem\SamCore\App\Traits\HasUuid;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int asset_id
 * @property int user_id
 * @property string key
 * @property string|null login
 * @property string|null pass
 * @property string|null description
 * @property Asset asset
 * @property \Totem\SamUsers\App\Model\User user
 */
class Meta extends Model
{
    use HasUuid,
        SoftDeletes;

    public function __construct(array $attributes = [])
    {
        $this->setAttribute('user_id', auth()->id());

        $this->fillable([
            'model_id',
            'model_type',
            'user_id',
            'key',
            'login',
            'password',
            'description',
        ]);

        $this->addHidden([
            'created_at',
            'updated_at',
            'model_id',
            'model_type',
            'user_id',
        ]);

        $this->setTable('asset_meta');

        parent::__construct($attributes);
    }

    public function model() : \Illuminate\Database\Eloquent\Relations\MorphTo
    {
        return $this->morphTo(__FUNCTION__, 'model', 'model_id')->withTrashed();
    }

    public function user() : \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(config('auth.providers.users.model'))->withTrashed();
    }

    public function setPasswordAttribute($value) : void
    {
        $this->attributes['password'] = encrypt($value, false);
    }

    public function getPasswordAttribute($value) : ?string
    {
        try {
            return decrypt($value, false);
        } catch (\Illuminate\Contracts\Encryption\DecryptException $e) {
            return null;
        }
    }
}