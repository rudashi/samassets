<?php

namespace Totem\SamAssets\App\Model;

use Totem\SamAssets\App\Enums\HardwareType;
use Chelout\RelationshipEvents\Concerns\HasMorphedByManyEvents;
use Totem\SamAssets\App\Model\Contracts\ParameterizableInterface;

class Hardware extends Asset implements ParameterizableInterface
{

    use HasMorphedByManyEvents;

    public function enumType() : string
    {
        return HardwareType::class;
    }

}