<?php

namespace Totem\SamAssets\App\Model;

use Totem\SamCore\App\Traits\HasUuid;
use Totem\SamAssets\App\Enums\LogType;
use Illuminate\Database\Eloquent\Model;

/**
 * @property \DateTime created_at
 * @property int id
 * @property string action
 * @property int user_id
 * @property int asset_id
 * @property int assignable_id
 * @property string assignable_type
 * @property \DateTime|null printed_at
 * @property Asset asset
 * @property Asset|Location|\Totem\SamCore\App\Model\File|\Totem\SamUsers\App\Model\User assigned
 * @property \Totem\SamUsers\App\Model\User user
 */
class Log extends Model
{
    use HasUuid;

    public const UPDATED_AT = null;

    protected $casts = [
        'printed_at' => 'date|Y-m-d H:i:s'
    ];

    public function __construct(array $attributes = [])
    {
        $this->fillable([
            'action',
            'user_id',
            'asset_id',
            'assignable_id',
            'assignable_type',
            'printed_at',
        ]);

        $this->addHidden([
            'user_id',
            'asset_id',
            'assignable_id',
            'assignable_type',
        ]);

        $this->setTable('asset_logs');

        parent::__construct($attributes);
    }

    public function asset() : \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Asset::class, 'asset_id')->withTrashed();
    }

    public function user() : \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(config('auth.providers.users.model'))->withTrashed();
    }

    public function assigned() : \Illuminate\Database\Eloquent\Relations\MorphTo
    {
        return $this->morphTo('assigned', 'assignable_type', 'assignable_id')->withTrashed();
    }

    public function getActionAttribute($action) : string
    {
        return LogType::getValue($action);
    }

}
