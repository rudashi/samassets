<?php

namespace Totem\SamAssets\App\Model;

use Totem\SamAssets\App\Classes\StorageCollection;
use Totem\SamAssets\App\Observer\LocationObserver;
use Illuminate\Database\Eloquent\SoftDeletes;
use Chelout\RelationshipEvents\Concerns\HasBelongsToManyEvents;
use Chelout\RelationshipEvents\Traits\HasRelationshipObservables;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int id
 * @property string|null name
 * @property int|null parent_id
 * @property string|null street
 * @property string|null street_number
 * @property string|null place_number
 * @property string|null post_code
 * @property string|null city
 * @property string|null country_code
 * @property int|null manager_id
 *
 * @property string|null image
 * @property string|null address
 * @property \Illuminate\Database\Eloquent\Collection assets_collection
 *
 * @property \Illuminate\Database\Eloquent\Collection assets
 * @property int|null assets_count
 * @property Location parent
 * @property \Illuminate\Database\Eloquent\Collection children
 * @property Log history
 * @property \Totem\SamUsers\App\Model\User manager
 */
class Location extends Model
{
    use SoftDeletes,
        HasRelationshipObservables,
        HasBelongsToManyEvents;

    public function __construct(array $attributes = [])
    {
        $this->fillable([
            'name',
            'parent_id',
            'manager_id',
            'street',
            'street_number',
            'place_number',
            'post_code',
            'city',
            'country_code',
            'image',
        ]);

        $this->addHidden([
            'created_at',
            'updated_at',
            'deleted_at',
        ]);

        $this->setTable('asset_locations');

        parent::__construct($attributes);
    }

    public function getImageAttribute($image): string
    {
        if ($image !== null && is_file('storage/' . $image)) {
            return asset('storage/' . $image);
        }
        return url('images/empty-photo.svg');
    }

    public function getAddressAttribute(): string
    {
        return "$this->street $this->street_number $this->place_number";
    }

    public function getAssetsCollectionAttribute(): StorageCollection
    {
        $relation = $this->relationLoaded('assets') ? $this->getRelation('assets') : $this->assets;
        return new StorageCollection($relation->groupBy('category'));
    }

    public function assets(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Asset::class, 'asset_assign', 'assignable_id')->where('assignable_type', self::class);
    }

    public function parent(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(self::class, 'parent_id','id');
    }

    public function children(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(self::class, 'parent_id');
    }

    public function history(): \Illuminate\Database\Eloquent\Relations\MorphMany
    {
        return $this->morphMany(Log::class, 'assignable')->with(['user', 'asset'])->latest();
    }

    public function manager(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(config('auth.providers.users.model'), 'manager_id');
    }

    public function hasParent(int $id): bool
    {
        return $this->parent && $this->parent->id === $id;
    }

    public function hasManager(int $id): bool
    {
        return $this->manager && $this->manager->id === $id;
    }

    protected static function boot(): void
    {
        parent::boot();

        static::observe(LocationObserver::class);

    }

}
