<?php

namespace Totem\SamAssets\App\Model;

use Totem\SamAssets\App\Enums\AccessoryType;
use Totem\SamAssets\App\Enums\CardType;
use Totem\SamAssets\App\Enums\HardwareType;
use Totem\SamAssets\App\Enums\SoftwareType;
use Totem\SamAssets\App\Model\Contracts\ParameterizableInterface;
use Totem\SamAssets\App\Observer\AssetLogObserver;
use Totem\SamCore\App\Traits\HasFiles;
use Totem\SamAssets\App\Traits\HasMeta;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Totem\SamCore\App\Traits\EloquentDecoratorTrait;
use Chelout\RelationshipEvents\Concerns\HasMorphedByManyEvents;
use Chelout\RelationshipEvents\Traits\HasRelationshipObservables;
use Totem\SamCore\App\Repositories\Contracts\HasFilesInterface;
use Totem\SamCore\App\Traits\HasParameters;

/**
 * @property string name
 * @property string category
 * @property string type
 * @property string|null serial_number
 * @property \Illuminate\Database\Eloquent\Collection|Meta[] meta
 * @property string|null image
 * @property string|null invoice_number
 * @property \Illuminate\Support\Carbon|null invoice_date
 * @property string|null description
 * @property bool multi
 * @property bool active
 * @property string icon
 *
 * @property \Illuminate\Database\Eloquent\Collection users
 * @property \Illuminate\Database\Eloquent\Collection|Location[] locations
 * @property \Illuminate\Database\Eloquent\Collection|Log[] history
 * @property \Illuminate\Database\Eloquent\Collection|Asset[] assets
 *
 * @property \Totem\SamCore\App\Services\Parameters parameters
 *
 * @property bool has_locations
 * @property bool has_users
 * @property bool is_assignable
 * @property string category_name
 */
class Asset extends Model implements HasFilesInterface
{
    use SoftDeletes,
        EloquentDecoratorTrait,
        HasFiles,
        HasMeta,
        HasMorphedByManyEvents,
        HasRelationshipObservables,
        HasParameters;

    protected $casts = [
        'active' => 'bool',
        'multi' => 'bool',
        'parameters' => 'array',
    ];

    protected $dates = [
        'invoice_date' => 'datetime:Y-m-d',
    ];

    public function __construct(array $attributes = [])
    {
        $this->addHidden([
            'created_at',
            'updated_at',
            'deleted_at',
            'type',
            'pivot',
        ]);

        $this->fillable([
            'name',
            'category',
            'type',
            'serial_number',
            'parameters',
            'image',
            'invoice_number',
            'invoice_date',
            'description',
            'multi',
            'active',
        ]);

        $this->setTable('assets');

        self::$parameters_intersect = false;

        parent::__construct($attributes);
    }

    public function path() : string
    {
        return 'assets/files';
    }

    public function getImageAttribute($image) : string
    {
        if ($image !== null && is_file('storage/' . $image)) {
            return asset('storage/' . $image);
        }
        return url('images/empty-photo.svg');
    }

    public function getCategoryNameAttribute() : string
    {
        if ($this instanceof ParameterizableInterface) {
            if ($this->enumType()::hasKey($this->type)) {
                return $this->type;
            }

            if ($this->enumType()::hasValue($this->type)) {
                return $this->enumType()::getKey($this->type);
            }
        }

        return '';
    }

    public function getHasLocationsAttribute() : bool
    {
        return $this->locations->isNotEmpty();
    }

    public function getHasUsersAttribute() : bool
    {
        return $this->users->isNotEmpty();
    }

    public function getIsAssignableAttribute() : bool
    {
        return $this->has_locations === false && ($this->multi === true || $this->has_users === false);
    }

    public function users() : \Illuminate\Database\Eloquent\Relations\MorphToMany
    {
        return $this->morphedByMany(config('auth.providers.users.model'), 'assignable', 'asset_assign', 'asset_id');
    }

    public function locations() : \Illuminate\Database\Eloquent\Relations\MorphToMany
    {
        return $this->morphedByMany(Location::class, 'assignable', 'asset_assign', 'asset_id');
    }

    public function history() : \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Log::class, 'asset_id')->with(['user', 'assigned'])->latest();
    }

    public function newFromBuilder($attributes = [], $connection = null) : Model
    {
        return $this->decorator(
            'type',
            array_merge(
                HardwareType::toArray(),
                AccessoryType::toArray(),
                SoftwareType::toArray(),
                CardType::toArray()
            ),
            $attributes,
            $connection
        );
    }

    protected static function boot() : void
    {
        parent::boot();

        static::observe(AssetLogObserver::class);

        if (static::class !== self::class) {
            static::addGlobalScope('category', static function (\Illuminate\Database\Eloquent\Builder $builder) {
                $builder->where('category', '=', class_basename(static::class));
            });
        }
    }

}
