<?php

namespace Totem\SamAssets\App\Model\Types;

use Totem\SamAssets\App\Model\Software;

class License extends Software
{

    protected static array $parameters = [
        'inventory',
    ];

}
