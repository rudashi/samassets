<?php

namespace Totem\SamAssets\App\Model\Types;

use Totem\SamAssets\App\Model\Accessory;

class SmartTv extends Accessory
{

    protected static array $parameters = [
        'screen_size',
        'resolution',
        'format',
        'hdmi',
        'usb_c',
        'usb',
        'wifi_mac',
        'wifi_ip',
        'inventory',
    ];

}
