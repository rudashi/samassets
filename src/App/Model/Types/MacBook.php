<?php

namespace Totem\SamAssets\App\Model\Types;

use Totem\SamAssets\App\Model\Hardware;

class MacBook extends Hardware
{

    protected static array $parameters = [
        'tag',
        'system',
        'cpu',
        'hdd_1',
        'hdd_2',
        'ram',
        'lan_mac',
        'lan_ip',
        'wifi_mac',
        'wifi_ip',
        'inventory',
    ];

}
