<?php

namespace Totem\SamAssets\App\Model\Types;

use Totem\SamAssets\App\Model\Accessory;

class FuelCard extends Accessory
{

    protected static array $parameters = [
        'pin',
        'car_plate',
    ];

}
