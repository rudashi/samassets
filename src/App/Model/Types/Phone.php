<?php

namespace Totem\SamAssets\App\Model\Types;

use Totem\SamAssets\App\Model\Hardware;

class Phone extends Hardware
{

    protected static array $parameters = [
        'tag',
        'imei',
        'wifi_mac',
        'wifi_ip',
        'inventory',
    ];

}
