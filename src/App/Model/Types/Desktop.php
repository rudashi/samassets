<?php

namespace Totem\SamAssets\App\Model\Types;

use Totem\SamAssets\App\Model\Hardware;

class Desktop extends Hardware
{

    protected static array $parameters = [
        'tag',
        'ad',
        'system',
        'cpu',
        'hdd_1',
        'hdd_2',
        'ram',
        'lan_mac',
        'lan_ip',
        'lan2_mac',
        'lan2_ip',
        'inventory',
    ];

}
