<?php

namespace Totem\SamAssets\App\Model\Types;

use Totem\SamAssets\App\Model\Accessory;

class SimCard extends Accessory
{

    protected static array $parameters = [
        'number',
        'pin',
        'puk',
    ];

}
