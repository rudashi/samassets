<?php

namespace Totem\SamAssets\App\Model\Types;

use Totem\SamAssets\App\Model\Card;

class Key extends Card
{

    protected static array $parameters = [
        'access',
    ];

}
