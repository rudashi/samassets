<?php

namespace Totem\SamAssets\App\Model\Types;

use Totem\SamAssets\App\Model\Software;

class SubscriptionLicense extends Software
{

    protected static array $parameters = [
        'login',
        'password',
        'inventory',
    ];

}
