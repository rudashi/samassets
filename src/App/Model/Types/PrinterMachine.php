<?php

namespace Totem\SamAssets\App\Model\Types;

use Totem\SamAssets\App\Model\Accessory;

class PrinterMachine extends Accessory
{

    protected static array $parameters = [
        'toner',
        'drum',
        'wifi_mac',
        'wifi_ip',
        'inventory',
    ];

}
