<?php

namespace Totem\SamAssets\App\Model\Types;

use Totem\SamAssets\App\Model\Card;

class Keychain extends Card
{

    protected static array $parameters = [
        'color',
        'serial_short',
        'serial_extra',
        'access',
    ];

}
