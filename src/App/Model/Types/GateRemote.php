<?php

namespace Totem\SamAssets\App\Model\Types;

use Totem\SamAssets\App\Model\Accessory;

class GateRemote extends Accessory
{

    protected static array $parameters = [
        'id',
        'gate_1',
        'gate_2',
        'gate_3',
        'gate_4',
    ];

}
