<?php

namespace Totem\SamAssets\App\Model\Types;

use Totem\SamAssets\App\Model\Accessory;

class Monitor extends Accessory
{

    protected static array $parameters = [
        'screen_size',
        'resolution',
        'format',
        'hdmi',
        'usb_c',
        'display_port',
        'dvi',
        'vga',
        'usb',
        'speakers',
        'inventory',
    ];

}
