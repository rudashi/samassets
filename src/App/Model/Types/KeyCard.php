<?php

namespace Totem\SamAssets\App\Model\Types;

use Totem\SamAssets\App\Model\Card;

class KeyCard extends Card
{

    protected static array $parameters = [
        'serial_short',
        'serial_extra',
        'access',
    ];

}
