<?php

namespace Totem\SamAssets\App\Model\Types;

use Totem\SamAssets\App\Model\Hardware;

class Tablet extends Hardware
{

    protected static array $parameters = [
        'tag',
        'cpu',
        'hdd',
        'ram',
        'wifi_mac',
        'wifi_ip',
        'inventory',
    ];

}
