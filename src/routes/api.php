<?php

use Illuminate\Support\Facades\Route;
use Totem\SamAssets\App\Controllers\CardController;
use Totem\SamAssets\App\Controllers\AssetsController;
use Totem\SamAssets\App\Controllers\ActivityController;
use Totem\SamAssets\App\Controllers\HardwareController;
use Totem\SamAssets\App\Controllers\LocationController;
use Totem\SamAssets\App\Controllers\ProtocolController;
use Totem\SamAssets\App\Controllers\SoftwareController;
use Totem\SamAssets\App\Controllers\AccessoryController;
use Totem\SamAssets\App\Controllers\AssetUploadController;

Route::group(['prefix' => 'api' ], static function() {

    Route::middleware(config('sam-admin.guard-api'))->group(static function() {

        Route::group(['prefix' => 'assets'], static function() {

            Route::get('meta', [AssetsController::class, 'getMetaTypes'])->middleware('permission:assets.view');
            Route::get('unused', [AssetsController::class, 'unused'])->middleware('permission:assets.view');
            Route::patch('{id}', [AssetsController::class, 'update'])->middleware('permission:assets.check-in', 'permission:assets.check-out');


            Route::group(['middleware' => 'permission:assets.file-upload'], static function() {
                Route::post('{assetId}/file', [AssetUploadController::class, 'upload'])->where(['assetId' => '[0-9]+']);
                Route::delete('{assetId}/file/{fileId}', [AssetUploadController::class, 'remove'])->where(['assetId' => '[0-9]+','fileId' => '[0-9]+']);
            });

            Route::group(['prefix' => '{type}', 'middleware' => 'permission:assets.edit', 'where' => ['type' => 'software|hardware|cards|accessories']], static function() {
                Route::post('{assetId}/meta', [AssetsController::class, 'createMeta'])->where(['assetId' => '[0-9]+']);
                Route::put('{assetId}/meta/{uuid}', [AssetsController::class, 'replaceMeta'])->where(['assetId' => '[0-9]+']);
                Route::delete('{assetId}/meta/{uuid}', [AssetsController::class, 'destroyMeta'])->where(['assetId' => '[0-9]+']);
            });

            Route::group(['prefix' => 'activity', 'middleware' => 'permission:assets.view'], static function() {
                Route::get('/', [ActivityController::class, 'index']);
                Route::get('grouped', [ActivityController::class, 'grouped']);
            });

            Route::group(['prefix' => 'protocols', 'middleware' => 'permission:assets.protocols.view'], static function() {
                Route::get('/', [ProtocolController::class, 'index']);
                Route::post('pdf', [ProtocolController::class, 'streamPDF']);
                Route::patch('/', [ProtocolController::class, 'printing']);
            });

            Route::group(['prefix' => 'locations', 'middleware' => 'permission:assets.locations.view'], static function() {
                Route::get('/', [LocationController::class, 'index']);
                Route::post('/', [LocationController::class, 'create'])->middleware('permission:assets.create');

                Route::get('{id}', [LocationController::class, 'show'])->middleware('permission:assets.show');
                Route::put('{id}', [LocationController::class, 'replace'])->middleware('permission:assets.edit');
                Route::patch('{id}', [LocationController::class, 'update'])->middleware('permission:assets.edit');
                Route::delete('{id}', [LocationController::class, 'destroy'])->middleware('permission:assets.delete');
            });

            Route::group(['prefix' => 'hardware', 'middleware' => 'permission:assets.hardware.view'], static function() {
                Route::get('/', [HardwareController::class, 'index']);
                Route::post('/', [HardwareController::class, 'create'])->middleware('permission:assets.create');
                Route::get('types', [HardwareController::class, 'getTypes']);

                Route::get('{id}', [HardwareController::class, 'show'])->middleware('permission:assets.show');
                Route::put('{id}', [HardwareController::class, 'replace'])->middleware('permission:assets.edit');
                Route::patch('{id}', [HardwareController::class, 'update'])->middleware('permission:assets.check-in', 'permission:assets.check-out');
                Route::delete('{id}', [HardwareController::class, 'destroy'])->middleware('permission:assets.delete');
            });

            Route::group(['prefix' => 'accessories', 'middleware' => 'permission:assets.accessories.view'], static function() {
                Route::get('/', [AccessoryController::class, 'index']);
                Route::post('/', [AccessoryController::class, 'create'])->middleware('permission:assets.create');
                Route::get('types', [AccessoryController::class, 'getTypes']);

                Route::get('{id}', [AccessoryController::class, 'show'])->middleware('permission:assets.show');
                Route::put('{id}', [AccessoryController::class, 'replace'])->middleware('permission:assets.edit');
                Route::patch('{id}', [AccessoryController::class, 'update'])->middleware('permission:assets.check-in', 'permission:assets.check-out');
                Route::delete('{id}', [AccessoryController::class, 'destroy'])->middleware('permission:assets.delete');
            });

            Route::group(['prefix' => 'software', 'middleware' => 'permission:assets.software.view'], static function() {
                Route::get('/', [SoftwareController::class, 'index']);
                Route::post('/', [SoftwareController::class, 'create'])->middleware('permission:assets.create');
                Route::get('types', [SoftwareController::class, 'getTypes']);

                Route::get('{id}', [SoftwareController::class, 'show'])->middleware('permission:assets.show');
                Route::put('{id}', [SoftwareController::class, 'replace'])->middleware('permission:assets.edit');
                Route::patch('{id}', [SoftwareController::class, 'update'])->middleware('permission:assets.check-in', 'permission:assets.check-out');
                Route::delete('{id}', [SoftwareController::class, 'destroy'])->middleware('permission:assets.delete');
            });

            Route::group(['prefix' => 'cards', 'middleware' => 'permission:assets.cards.view'], static function() {
                Route::get('/', [CardController::class, 'index']);
                Route::post('/', [CardController::class, 'create'])->middleware('permission:assets.create');
                Route::get('types', [CardController::class, 'getTypes']);

                Route::get('{id}', [CardController::class, 'show'])->middleware('permission:assets.show');
                Route::put('{id}', [CardController::class, 'replace'])->middleware('permission:assets.edit');
                Route::patch('{id}', [CardController::class, 'update'])->middleware('permission:assets.check-in', 'permission:assets.check-out');
                Route::delete('{id}', [CardController::class, 'destroy'])->middleware('permission:assets.delete');
            });
        });
    });

});
