<?php

namespace Totem\SamAssets\Database\Seeds;

use Illuminate\Database\Seeder;
use Totem\SamAcl\Database\PermissionTraitSeeder;

class PermissionSeeder extends Seeder
{

    use PermissionTraitSeeder;

    /**
     * Array of permissions.
     * %action  : view|create|edit|show|delete|modify
     * %name    : translatable from JSON
     *
     * @return array
     *  [
     *      'slug' => 'roles.modify',
     *      'name' => 'Can Modify Roles',
     *      'description' => 'Can modify roles',
     *  ]
     */
    public function permissions(): array
    {
        return [
            [
                'slug' => 'assets.view',
                'name' => 'Can View Assets',
                'description' => 'Can view assets',
            ],
            [
                'slug' => 'assets.create',
                'name' => 'Can Create Assets',
                'description' => 'Can create new assets',
            ],
            [
                'slug' => 'assets.edit',
                'name' => 'Can Edit Assets',
                'description' => 'Can edit assets',
            ],
            [
                'slug' => 'assets.show',
                'name' => 'Can Show Assets',
                'description' => 'Can show assets',
            ],
            [
                'slug' => 'assets.delete',
                'name' => 'Can Delete Assets',
                'description' => 'Can delete assets',
            ],

            [
                'slug' => 'assets.check-in',
                'name' => 'Can Check In Assets',
                'description' => 'Can check in an asset to user',
            ],
            [
                'slug' => 'assets.check-out',
                'name' => 'Can Check Out Assets',
                'description' => 'Can check out an asset to a user',
            ],

            [
                'slug' => 'assets.hardware.view',
                'name' => 'Can View Hardware',
                'description' => 'Can view hardware',
            ],
            [
                'slug' => 'assets.accessories.view',
                'name' => 'Can View Accessories',
                'description' => 'Can view accessories',
            ],
            [
                'slug' => 'assets.software.view',
                'name' => 'Can View Software',
                'description' => 'Can view software',
            ],
            [
                'slug' => 'assets.passwords.view',
                'name' => 'Can View Asset Passwords',
                'description' => 'Can view assigned password to asset',
            ],
            [
                'slug' => 'assets.file-upload',
                'name' => 'Can Upload Files',
                'description' => 'Can upload and remove files from assets',
            ],
            [
                'slug' => 'assets.cards.view',
                'name' => 'Can View Cards',
                'description' => 'Can view cards',
            ],
            [
                'slug' => 'assets.protocols.view',
                'name' => 'Can View Protocols',
                'description' => 'Can view protocol view',
            ],
            [
                'slug' => 'assets.locations.view',
                'name' => 'Can View Locations',
                'description' => 'Can view locations',
            ],

            [
                'slug' => 'assets.log.notify.card',
                'name' => 'Notification of card logs',
                'description' => null,
            ],
        ];
    }

}
