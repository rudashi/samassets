<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AssetsMeta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function up() : void
    {
        try{
            Schema::create('asset_meta', static function (Blueprint $table) {
                $table->uuid('id');
                $table->timestamps();
                $table->morphs('model');
                $table->integer('user_id')->unsigned();
                $table->string('key');
                $table->string('login')->nullable();
                $table->string('password')->nullable();
                $table->string('description')->nullable();
                $table->softDeletes();

                $table->foreign('user_id')->references('id')->on('users')
                    ->onUpdate('cascade')->onDelete('cascade');
            });

        } catch (PDOException $ex) {
            $this->down();
            throw $ex;
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down() : void
    {
        Schema::dropIfExists('asset_meta');
    }
}
