<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AssetsSetup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function up() : void
    {
        try{
            Schema::create('assets', static function (Blueprint $table) {
                $table->increments('id');
                $table->timestamps();
                $table->string('name');
                $table->string('category');
                $table->string('type');
                $table->string('serial_number')->nullable();
                $table->json('parameters');
                $table->string('image')->nullable();
                $table->string('invoice_number')->nullable();
                $table->date('invoice_date')->nullable();
                $table->text('description')->nullable();
                $table->tinyInteger('multi')->default(0);
                $table->tinyInteger('active')->default(1);
                $table->softDeletes();
            });

            Schema::create('asset_logs', static function (Blueprint $table) {
                $table->uuid('id')->primary();
                $table->timestamp('created_at')->nullable();
                $table->string('action');
                $table->integer('user_id')->unsigned();
                $table->integer('asset_id')->unsigned();
                $table->morphs('assignable');
                $table->date('printed_at')->nullable();

                $table->foreign('user_id')->references('id')->on('users')
                    ->onUpdate('cascade')->onDelete('cascade');
                $table->foreign('asset_id')->references('id')->on('assets')
                    ->onUpdate('cascade')->onDelete('cascade');

            });

            Schema::create('asset_assign', static function (Blueprint $table) {
                $table->integer('asset_id')->unsigned();
                $table->morphs('assignable');

                $table->foreign('asset_id')->references('id')->on('assets')
                    ->onUpdate('cascade')->onDelete('cascade');
            });

        } catch (PDOException $ex) {
            $this->down();
            throw $ex;
        }

        Artisan::call('db:seed', [
            '--class' => \Totem\SamAssets\Database\Seeds\PermissionSeeder::class
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down() : void
    {
        Schema::dropIfExists('asset_logs');
        Schema::dropIfExists('asset_assign');
        Schema::dropIfExists('assets');

        (new \Totem\SamAssets\Database\Seeds\PermissionSeeder)->down();
    }
}
