<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AssetsExt extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function up() : void
    {
        try{
            Schema::create('asset_locations', static function (Blueprint $table) {
                $table->increments('id');
                $table->timestamps();
                $table->string('name')->nullable();
                $table->integer('parent_id')->unsigned()->nullable();
                $table->string('street', 80)->nullable();
                $table->string('street_number', 80)->nullable();
                $table->string('place_number', 80)->nullable();
                $table->string('post_code', 10)->nullable();
                $table->string('city')->nullable();
                $table->string('country_code',2)->nullable();
                $table->integer('manager_id')->unsigned()->nullable();
                $table->string('image')->nullable();
                $table->softDeletes();

                $table->foreign('manager_id')->references('id')->on('users')
                    ->onUpdate('cascade')->onDelete('cascade');

                $table->foreign('parent_id')->references('id')->on('asset_locations')
                    ->onUpdate('cascade')->onDelete('cascade');
            });

        } catch (PDOException $ex) {
            $this->down();
            throw $ex;
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     * @throws Exception
     */
    public function down() : void
    {
        Schema::dropIfExists('asset_locations');
    }
}
