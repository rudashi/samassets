<?php

use Faker\Generator as Faker;

/**
 * @var $factory \Illuminate\Database\Eloquent\Factory
 */
$factory->define(\Totem\SamAssets\App\Model\Card::class, static function (Faker $faker) {

    return [
        'name' => $faker->randomNumber(),
        'category' => \Totem\SamAssets\App\Enums\AssetCategory::Card,
        'type' => \Totem\SamAssets\App\Enums\CardType::KeyCard,
        'parameters' => [
            'access' => [],
            'serial_short' => $faker->creditCardNumber
        ],
        'description' => $faker->optional(0.3)->text,
    ];

});
