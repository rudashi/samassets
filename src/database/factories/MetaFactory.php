<?php

use Faker\Generator as Faker;

/**
 * @var $factory \Illuminate\Database\Eloquent\Factory
 */
$factory->define(\Totem\SamAssets\App\Model\Meta::class, static function (Faker $faker) {

    return [
        'user_id' => $faker->numberBetween(1, 10),
        'key' => $faker->slug,
        'login' => $faker->optional(0.3)->userName,
        'password' => $faker->optional(0.3)->password(6, 8),
        'description' => $faker->optional(0.3)->text,
    ];

});