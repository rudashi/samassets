<?php

use Faker\Generator as Faker;

/**
 * @var $factory \Illuminate\Database\Eloquent\Factory
 */
$factory->define(\Totem\SamAssets\App\Model\Location::class, function (Faker $faker) {

    return [
        'name' => $faker->unique()->domainWord,
        'street' => $faker->streetName,
        'street_number' => $faker->streetSuffix,
        'post_code' => $faker->postcode,
        'city' => $faker->city,
    ];
});