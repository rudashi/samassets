<?php

use Faker\Generator as Faker;
use Totem\SamAssets\App\Enums\AssetCategory;
use Totem\SamAssets\App\Enums\HardwareType;

/**
 * @var $factory \Illuminate\Database\Eloquent\Factory
 */
$factory->define(\Totem\SamAssets\App\Model\Asset::class, static function (Faker $faker) {
    $type = $faker->randomElement(AssetCategory::getKeys());
    $category = '\\Totem\\SamAssets\\App\\Enums\\'.$type.'Type';

    return [
        'name' => $faker->slug,
        'type' => $faker->randomElement($category::getKeys()),
        'category' => $type,
        'serial_number' => $faker->optional(0.3)->creditCardNumber,
        'parameters' => [],
        'invoice_number' => $faker->optional(0.3)->bankAccountNumber,
        'invoice_date' => $faker->optional(0.3)->date(),
        'description' => $faker->optional(0.3)->text,
        'multi' => $faker->numberBetween(0, 1),
        'active' => $faker->numberBetween(0, 1),
    ];

});

$factory->define(\Totem\SamAssets\App\Model\Types\Notebook::class, static function (Faker $faker) {

    return [
        'name' => $faker->slug,
        'category' => AssetCategory::getKey(AssetCategory::Hardware),
        'type' => HardwareType::getKey(HardwareType::Notebook),
        'serial_number' => $faker->optional(0.3)->creditCardNumber,
        'parameters' => [
            'cpu' => 'Intel Core i7-8565U',
            'ram' => 16,
            'tag' => 'Asus-MNadzieja',
            'hdd_1' => 512,
            'system' => 'Windows 10 Pro',
            'wifi_mac' => '38:00:25:51:70:9D',
        ],
        'invoice_number' => $faker->optional(0.3)->bankAccountNumber,
        'invoice_date' => $faker->optional(0.3)->date(),
        'description' => $faker->optional(0.3)->text,
        'multi' => $faker->numberBetween(0, 1),
        'active' => $faker->numberBetween(0, 1),
    ];
});
