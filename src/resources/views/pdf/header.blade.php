<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <style>
        .totem {
            height: 80px;
        }
        .totem.dark path {
            fill: #003b71 !important;
        }
    </style>
</head>
<body>
    @svg('images/logo-totem.svg', 'totem dark')
    <hr>
</body>
</html>