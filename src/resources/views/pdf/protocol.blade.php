<!doctype html>
<html lang="pl">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ __('Assets protocol') }}</title>

    <link href="https://fonts.googleapis.com/css?family=PT+Sans+Narrow&subset=latin-ext" rel="stylesheet">
    <style>
        * {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }
        html,body {
            height: 100%;
            margin: 0;
            font-size: 1rem;
            font-family: 'PT Sans Narrow', sans-serif;
        }
        .page {
            overflow: hidden;
            page-break-after: always;
            height: 290mm;

            display: -webkit-box;
            display: flex;
            -webkit-box-direction: normal;
            -webkit-box-orient: vertical;
            flex-direction: column;
            -webkit-box-pack: justify;
            justify-content: space-between;
        }
        .smaller {
            font-size: 0.8rem;
        }
        header {
            padding: 5mm 0 ;
            font-size: 1.5rem;
            text-align: center;
        }
        .main {
            font-size: 1.3rem;
            padding: 0 20mm;
        }
        h3 {
            text-decoration: underline;
        }
        section p {
            display: block;
            border: 1px solid #000;
            padding: 10% 0;
        }
        footer {
            display: flex;
            display: -webkit-box;
            justify-content: space-between;
            -webkit-box-pack: justify;
            margin-top: 20mm;
        }
        footer div {
            min-width: 25%;
            text-align: center;
        }
        footer div p {
            border-bottom: 1px dotted #000;
        }
        footer div span {
            white-space: nowrap;
        }
    </style>
</head>
<body>
    @isset($activities['checked out'])
        @foreach($activities['checked out'] as $data)
            <div class="page">
                @include('sam-assets::pdf.partial-check-out', ['data' => $data, 'date' => $date])
            </div>
        @endforeach
    @endisset

    @isset($activities['checked in'])
        @foreach($activities['checked in'] as $data)
            <div class="page">
                @include('sam-assets::pdf.partial-check-in', ['data' => $data, 'date' => $date])
            </div>
            @endforeach
    @endisset
</body>
</html>