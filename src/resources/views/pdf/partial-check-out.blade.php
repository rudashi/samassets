<div>
    <header>
        <h1>{{ __('Assets return protocol') }}</h1>
    </header>
    <div class="main">
        <p>Sporządzony w dniu: {{ $date->format('d.m.Y') }} r.</p>
        <p>Pracownik <strong>{{ $data[0]->assigned->fullname }}</strong> potwierdza, że zdał: </p>
        <ul>
            @foreach($data as $item)
                @if($item->asset->category === 'Card')
                <li>Identyfikator nr {{ $item->asset->name }}</li>
                @else
                <li>{{ $item->asset->name }} {{ $item->asset->serial_number ? "({$item->asset->serial_number})" : '' }}</li>
                @endif
            @endforeach
        </ul>
    </div>
</div>
<div>
    <section>
        <h3>Uwagi:</h3>
        <p>&nbsp;</p>
    </section>
    <footer>
        <div>
            <p>&nbsp;</p>
            <span>Podpis osoby upoważnionej do odbioru</span>
        </div>
        <div>
            <p>&nbsp;</p>
            <span>Podpis osoby zdającej</span>
        </div>
    </footer>
</div>