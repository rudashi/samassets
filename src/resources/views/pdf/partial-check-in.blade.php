<div>
    <header>
        <h1>{{ __('Assets transfer protocol') }}</h1>
    </header>
    <div class="main">
        <p>Sporządzony w dniu: {{ $date->format('d.m.Y') }} r.</p>
        <p>Pracownik <strong>{{ $data[0]->assigned->fullname }}</strong> potwierdza, że otrzymał: </p>
        <ul>
            @foreach($data as $item)
                @if($item->asset->category === 'Card')
                    <li>Identyfikator nr {{ $item->asset->name }}</li>
                @else
                    <li>{{ $item->asset->name }} {{ $item->asset->serial_number ? "({$item->asset->serial_number})" : '' }}</li>
                @endif
            @endforeach
        </ul>
        <p>i oświadcza, że nie będzie udostępniał mienia osobom nieupoważnionym.</p>
        <p>Stan: <strong>nowy / używany</strong></p>
        <div class="smaller">
            <h3>Pouczenie:</h3>
            <p>Pracownik, któremu powierzono mienie, ponosi za nie odpowiedzialność na podstawie art. 124 Kp.
                Za szkodę w mieniu powierzonym pracownik odpowiada w pełnej wysokości niezależnie od tego, czy szkoda
                powstała z winy umyślnej, czy też nieumyślnej. Przez odpowiedzialność w pełnej wysokości, rozumie się
                odpowiedzialność w granicach rzeczywistej straty i utraconych korzyści. </p>
            <p>W związku z przyjęciem odpowiedzialności materialnej za wskazane wyżej mienie, Pracownik zobowiązany jest do:</p>
            <ul>
                <li>sprawowania pieczy nad powierzonym mu mieniem,</li>
                <li>po zapoznaniu się z instrukcją obsługi oraz zgodnie z przepisami bhp, do wykorzystywania powierzonego mienia zgodnie z jego przeznaczeniem i tylko do celów służbowych,</li>
                <li>niezwłocznego informowania bezpośredniego przełożonego o uszkodzeniu lub utracie powierzonego mienia,</li>
                <li>rozliczenia się z powierzonego mienia na wezwanie pracodawcy,</li>
                <li>wyrównania wszelkich szkód powstałych w mieniu powierzonym, wynikłych z winy pracownika.</li>
            </ul>
        </div>
    </div>
</div>
<div>
    <section>
        <h3>Uwagi:</h3>
        <p>&nbsp;</p>
    </section>
    <footer>
        <div>
            <p>&nbsp;</p>
            <span>Przekazujący</span>
        </div>
        <div>
            <p>&nbsp;</p>
            <span>Podpis Pracownika</span>
        </div>
    </footer>
</div>