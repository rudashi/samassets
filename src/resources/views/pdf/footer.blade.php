<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
    <style>
        p {
            text-align: center;
            color: #999999;
        }
        span {
            color: #1c94c4;
        }
    </style>
</head>
<body>
    <p>
        <span>TOTEM.COM.PL SPÓŁKA Z OGRANICZONĄ ODPOWIEDZIALNOŚCIĄ SPÓŁKA KOMANDYTOWA</span><br>
        • UL. JACEWSKA 89 • 88-100 INOWROCŁAW • POLSKA • TEL. +48 52 35 400 40 • TOTEM@TOTEM.COM.PL •<br>
        • TOTEM.COM.PL • VAT-ID: PL 5561007611 • REGON: 091187826 • KRS: 0000496427 •<br>
        • SĄD REJONOWY W BYDGOSZCZY, XIII WYDZIAŁ GOSPODARCZY KRAJOWEGO REJESTRU SĄDOWEGO •
    </p>
</body>
</html>