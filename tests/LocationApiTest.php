<?php

namespace Tests;

use Illuminate\Foundation\Testing\WithoutEvents;
use Totem\SamAcl\Testing\AttachRoleToUserTrait;
use Totem\SamAcl\Testing\CheckForbiddenEndpoints;
use Totem\SamAdmin\Testing\ApiCrudTest;
use Totem\SamAssets\App\Enums\LogType;
use Totem\SamAssets\App\Model\Asset;
use Totem\SamAssets\App\Model\Location;
use Totem\SamAssets\App\Model\Types\Notebook;

class LocationApiTest extends ApiCrudTest
{

    use AttachRoleToUserTrait,
        CheckForbiddenEndpoints,
        WithoutEvents;

    protected string $endpoint = 'assets/locations';
    protected string $model = Location::class;
    protected array $withoutFields = [
        'name'
    ];

    protected function createModel(array $attributes = []): Location
    {
        return factory($this->model)->create($attributes);
    }

    public function test_get_all(): void
    {
        $model = $this->createModel();

        $this->get("/api/$this->endpoint")
            ->assertOk()
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'id',
                        'name',
                        'parent_id',
                        'street',
                        'street_number',
                        'place_number',
                        'post_code',
                        'city',
                        'country_code',
                        'manager_id',
                        'image',
                    ]
                ],
                'apiVersion'
            ])
            ->assertJsonFragment([
                'id' => $model->id,
                'name' => $model->name,
                'parent_id' => null,
                'street' => $model->street,
                'street_number' => $model->street_number,
                'place_number' => null,
                'post_code' => $model->post_code,
                'city' => $model->city,
                'country_code' => null,
                'manager_id' => null,
                'image' => $model->image,
            ]);
    }

    public function test_assign_asset_to_location(): void
    {
        /** @var Asset $asset */
        $model = $this->createModel();
        $asset = factory(Notebook::class)->create();

        $this->patch("/api/$this->endpoint/$model->id", ['action' => LogType::getKey(LogType::assign), 'asset_id' => $asset->id])
            ->assertOk()
            ->assertJsonFragment([
                'id' => $asset->id,
                'name' => $asset->name,
                'type' => $asset->category,
                'category' => $asset->category_name,
                'serial_number' => $asset->serial_number,
                'image' => $asset->image,
                'invoice_number' => $asset->invoice_number,
                'invoice_date' => $asset->invoice_date,
                'description' => $asset->description,
                'multi' => $asset->multi,
                'locations' => [
                    [
                        'id' => $model->id,
                        'name' => $model->name,
                        'parent_id' => null,
                        'street' => $model->street,
                        'street_number' => $model->street_number,
                        'place_number' => null,
                        'post_code' => $model->post_code,
                        'city' => $model->city,
                        'country_code' => null,
                        'manager_id' => null,
                        'image' => $model->image,
                    ]
                ],
                'active' => $asset->active,
            ]);
    }

    public function test_failed_already_assigned_asset_to_location(): void
    {
        /** @var Asset $asset */
        $model = $this->createModel();
        $asset = factory(Notebook::class)->create();
        $asset->locations()->save($model);

        $this->patch("/api/$this->endpoint/$model->id", ['action' => LogType::getKey(LogType::assign), 'asset_id' => $asset->id])
            ->assertStatus(422)
            ->assertJsonFragment([
                __('The asset :name is already assigned.', ['name' => $asset->name])
            ]);
    }

    public function test_withdrawn_asset_from_location(): void
    {
        /** @var Asset $asset */
        $model = $this->createModel();
        $asset = factory(Notebook::class)->create();
        $asset->locations()->save($model);

        $this->patch("/api/$this->endpoint/$model->id", ['action' => LogType::getKey(LogType::withdrawn), 'asset_id' => $asset->id])
            ->assertOk()
            ->assertJsonFragment([
                'id' => $asset->id,
                'name' => $asset->name,
                'type' => $asset->category,
                'category' => $asset->category_name,
                'serial_number' => $asset->serial_number,
                'image' => $asset->image,
                'invoice_number' => $asset->invoice_number,
                'invoice_date' => $asset->invoice_date,
                'description' => $asset->description,
                'multi' => $asset->multi,
                'locations' => [],
                'active' => $asset->active,
            ]);
    }

    public function test_failed_withdrawn_not_assigned_asset_from_location(): void
    {
        /** @var Asset $asset */
        $model = $this->createModel();
        $asset = factory(Notebook::class)->create();

        $this->patch("/api/$this->endpoint/$model->id", ['action' => LogType::getKey(LogType::withdrawn), 'asset_id' => $asset->id])
            ->assertStatus(422)
            ->assertJsonFragment([
                __('The asset :name is not assigned.', ['name' => $asset->name])
            ]);
    }

    public function test_failed_action_missing_parameter(): void
    {
        $this->patch("/api/$this->endpoint/1", ['action' => 'bla bla', 'asset_id' =>1])
            ->assertStatus(422)
            ->assertJsonFragment([
                'Missing Parameter.'
            ]);
    }


}
