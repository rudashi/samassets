<?php

namespace Tests;

use Illuminate\Support\Facades\Notification;
use Totem\SamAcl\App\Model\Permission;
use Totem\SamAssets\App\Notifications\AssetLogCreated;
use Totem\SamUsers\App\Model\User;
use Totem\SamAssets\App\Model\Card;
use Totem\SamAssets\App\Repositories\AssetsRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ActionAssetTest extends TestCase
{
    use DatabaseTransactions;

    public function test_assign_user_to_asset() : void
    {
        $relationName = 'users';
        $result = $this->attachAsset($relationName);

        $this->assertTrue($result->relationLoaded($relationName));
        $this->assertInstanceOf(\Illuminate\Database\Eloquent\Collection::class, $result->{$relationName});
        $this->assertNotEmpty($result->{$relationName});
        $this->assertInstanceOf(User::class, $result->{$relationName}->first());
    }

    public function test_send_log_notification(): void
    {
        Notification::fake();

        $permission = Permission::query()->firstWhere('slug', 'assets.log.notify.card');
        $user = User::query()->inRandomOrder()->get()->first();
        $user->attachPermission($permission);

        $asset = $this->attachAsset('users', $user);

        Notification::assertSentTo(
            auth()->user(),
            AssetLogCreated::class,
            function (AssetLogCreated $notification, array $channels, $notifiable) use ($user, $asset) {
                return $notifiable->id === $user->id
                    && $notification->log->asset->id === $asset->id
                ;
            }
        );
    }

    private function attachAsset(string $relationName, User $user = null): \Totem\SamAssets\App\Model\Asset
    {
        $asset = factory(Card::class)->create();
        $user = $user ?? User::query()->inRandomOrder()->get()->first();
        $this->actingAs($user, 'web');

        $repository = new AssetsRepository();
        return $repository->attach($relationName, $user, $asset);
    }
}
