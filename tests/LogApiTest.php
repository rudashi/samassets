<?php

namespace Tests;

use Totem\SamAcl\Testing\AttachRoleToUserTrait;
use Totem\SamAdmin\Testing\ApiTest;
use Totem\SamAssets\App\Model\Log;

class LogApiTest extends ApiTest
{
    use AttachRoleToUserTrait;

    protected string $endpoint = 'assets/activity';
    protected string $model = Log::class;

    protected function createModel(array $attributes = []): Log
    {
        return factory($this->model)->create($attributes);
    }

    public function test_get_all_activity(): void
    {
        $this->get("/api/$this->endpoint")
            ->assertOk()
            ->assertJsonStructure([
                'data',
                'apiVersion'
            ]);
    }

    public function test_get_all_grouped(): void
    {
        $this->get("/api/$this->endpoint/grouped")
            ->assertOk()
            ->assertJsonStructure([
                'data',
                'apiVersion'
            ]);
    }

}
