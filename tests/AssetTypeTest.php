<?php

namespace Tests;

use Illuminate\Support\Collection;
use Totem\SamAssets\App\Model\Meta;
use Totem\SamCore\App\Services\Parameters;
use Totem\SamAssets\App\Model\Types\Notebook;

class AssetTypeTest extends TestCase
{

    public function test_notebook_model(): void
    {
        /** @var Notebook $model */
        $model = factory(Notebook::class)->make();
        $model->meta->add(factory(Meta::class)->make());

        self::assertInstanceOf(Notebook::class, $model);
        self::assertInstanceOf(Parameters::class, $model->parameters);
        self::assertSame($model->parameters->ram, 16);
        self::assertInstanceOf(Collection::class, $model->meta);
        self::assertInstanceOf(Meta::class, $model->meta->first());
    }

}
