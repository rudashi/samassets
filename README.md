Sam Assets Component
================

This package manages assets and IT operations in SAM applications.

![Totem.com.pl](https://www.totem.com.pl/wp-content/uploads/2016/06/logo.png)

## General System Requirements

- [PHP >7.4.0](http://php.net/)
- [Laravel ~6.*](https://github.com/laravel/framework)
- [SAM-core ~1.*](https://bitbucket.org/rudashi/samcore)  
- [SAM-Users ~1.*](https://bitbucket.org/rudashi/samusers)  

## Quick Installation

If necessary, use the composer to download the library

```
$ composer require totem-it/sam-assets
```

```
"repositories": [
    {
        "type": "vcs",
        "url":  "https://bitbucket.org/rudashi/samassets.git"
    }
],
```

Add the trait to `User` model (default `App\User`)

```php
use Totem\SamAssets\App\Traits\HasAssets;
```

Finally, you'll need also to run migration
```
php artisan migrate
```

## Usage

### API

Check [openapi](openapi.json) file.

Endpoints use standard filtering from `SAM-core`.

## Authors

* **Borys Żmuda** - Lead designer - [LinkedIn](https://www.linkedin.com/in/boryszmuda/), [Portfolio](https://rudashi.github.io/)
